<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')

	<!-- Content Header (Page header) -->
	<section class="content-header hidden-xs">
		<h1>
			CMS
			<small>Content Management System</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ URL::route('editor.payroll.index') }}"><i class="fa fa-list-ul"></i> Payroll List</a></li>
			<li class="active">
				@if(isset($payroll))
				<i class="fa fa-pencil"></i> Edit
				@else
				<i class="fa fa-plus"></i> Create
				@endif
			</li>
		</ol>
	</section>
	@actionStart('payroll', 'create|update')
	<section class="content">
		<section class="content box box-solid">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="x_panel">
							<h2>
								@if(isset($payroll))
								<i class="fa fa-pencil"></i>
								@else
								<i class="fa fa-plus"></i>
								@endif
								&nbsp;Payroll List
							</h2>
						</div>
						<hr>
						<div class="col-md-12">
							@include('errors.error')
							{!! Form::open(array('route' => 'editor.payroll.storeheader', 'class'=>'create', 'id' => 'form_payroll'))!!}
							{{ csrf_field() }}
							<div class="x_content">
								<div class="col-md-4">
									{{ Form::label('year', 'Year') }}
									{{ Form::number('year', old('year', date('Y')), ['class' => 'form-control', 'id' => 'year', 'placeholder' => 'Year of Payroll', 'min' => (date('Y') - 20), 'max' => (date('Y') + 20)]) }}
									<br>
								</div>


								<div class="col-md-4">
									{{ Form::label('month', 'Month') }}
									{{ Form::select('month', $month_list, old('month'), ['class' => 'form-control', 'id' => 'month', 'placeholder' => 'Month of Payroll']) }}
									<br>
								</div>
								<div class="col-md-4">
									{{ Form::label('default_work_days', 'Default Work Days') }}
									{{ Form::number('default_work_days', old('default_work_days'), ['class' => 'form-control', 'id' => 'default_work_days', 'placeholder' => 'Default Work Days','required' => 'true']) }}
									<br>
								</div>
								<div class="col-md-12">
									{{ Form::label('comment', 'Comment') }}
									{{ Form::text('comment', old('comment'), ['class' => 'form-control', 'id' => 'comment', 'placeholder' => 'Comment']) }}
									<br/>

									<button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success btn-lg pull-right"><i class="fa fa-check"></i> Next</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	@stop
	@actionEnd

@section('modal')
<div class="modal fade" id="modal_confirm">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excecute this payroll?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> Execute</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">

	$('#btn_submit').on('click', function()
	{
		$('#form_payroll').submit();
	});
</script>
@stop