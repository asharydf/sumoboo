<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.cashbond.index') }}"><i class="fa fa-dollar"></i> Cashbond</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Cashbond Finalized
						</h2>
					</div>
					<hr>
					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								@include('errors.error')
								{!! Form::model($cashbond, array('route' => ['editor.cashbond.storeclosing', $cashbond->id],  'class'=>'create', 'id' =>'form_cashbondclose', 'files' => 'true'))!!}
								{{ csrf_field() }}

								{{ Form::label('start_date', 'Start Date') }}
								<input type="text" class="form-control" name="start_date" value="{{date('d-M-Y', strtotime($cashbond->start_date))}}" disabled="disabled"><br/>

								{{ Form::label('end_date', 'End Date') }}
								<input type="text" class="form-control" name="end_date" value="{{date('d-M-Y', strtotime($cashbond->end_date))}}" disabled="disabled"><br/>

								{{ Form::label('budget_request', 'Budget Request') }}
								<input type="text" class="form-control" name="budget_request" value="{{number_format($cashbond->budget_request,0)}}" disabled="disabled"><br/>

								{{ Form::label('budget_issued', 'Budget Issued') }}
								<input type="text" class="form-control" name="budget_issued" value="{{number_format($cashbond->budget_issued,0)}}" disabled="disabled"><br/>

								{{ Form::label('cashout', 'Cash Out') }}
								<input type="text" class="form-control" name="cashout" value="{{number_format($cashbond->cashout,0)}}" disabled="disabled"><br/>

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true', 'disabled' => 'disabled')) }}  <br>

								<div style="width: 100%; border-bottom: 1px solid black; text-align: center">
									<span style="font-size: 20px; padding: 0 10px;">
										Section Detail <!--Padding is optional-->
									</span>
								</div>
								{{ Form::hidden('id', old('id')) }}
								{{ Form::label('date', 'Date') }}
								{{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date*', 'required' => 'date', 'id' => 'date')) }}<br/>


								{{ Form::label('nota_number', 'Nota Number') }}
								{{ Form::text('nota_number', old('nota_number'), array('class' => 'form-control', 'placeholder' => 'Nota Number*', 'required' => 'nota_number')) }}<br/>

								{{ Form::label('amount', 'Amount') }}

								@if($cashbond->cashbond_type == 'maintenance')
									{{ Form::number('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Amount*', 'required' => 'amount')) }}<br/>
								@else
									{{ Form::number('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Amount*', 'required' => 'amount', 'disabled' => 'disabled')) }}<br/>
								@endif


								{{ Form::label('description', 'Description') }}
								{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'description')) }}<br/>

								{{ Form::label('image', 'Attachment') }}
	                        	{{ Form::file('image') }}<br/>

								<button type="button" data-toggle="modal" data-target="#modal_cashbondclose" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Submit</button>
								<a href="{{ URL::route('editor.cashbond.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a><br>

								{!! Form::close() !!}
								<hr>
								<div class="div_overflow">
									<table id="invoiceTablex" class="table dataTable rwd-table">
										<thead>
											<tr>
												<th>#</th>
												<th>Date</th>
												<th>Nota Number</th>
												<th>Amount</th>
												<th>Attachment</th>
												<th>Item</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@foreach($cashbond_detail as $key => $cashbond_details)
											<tr>
												<td>{{$key+1}}</td>
												<td>{{$cashbond_details->date}}</td>
												<td>{{$cashbond_details->nota_number}}</td>
												<td>{{ number_format($cashbond_details->amount,0) }}</td>
												<td data-th="Attachment">
													@if($cashbond_details->cashbond_attachment == null)
													Tidak ada lampiran
													@else
													<a target="_blank" href="{{Config::get('constants.path.uploads')}}/cashbond_detail/{{$cashbond_details->cashbond_attachment}}"><i class="fa fa-download"></i>&nbsp;Download</a>

													@endif
												</td>
													@if($cashbond->cashbond_type != 'maintenance')
															<td>{{$cashbond_details->count_item}}</td>
												    @else
															<td></td>
												    @endif

													<td align="center">
															@if($cashbond->cashbond_type != 'maintenance')
															<a href="{{ URL::route('editor.cashbond.detailitem', [$cashbond_details->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-link"></i> Item</a>

														    @endif

															{!! Form::open(array('route' => ['editor.cashbond.deletedetail', $cashbond_details->id], 'method' => 'delete', 'class'=>'delete'))!!}
															{{ csrf_field() }}	&nbsp;

															<button type="submit" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a></button>
															{!! Form::close() !!}

													</td>


											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						{!! Form::open(array('route' => ['editor.cashbond.updatewaitingverified', $cashbond->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_cashbondconfirm'))!!}
						{{ csrf_field() }}

						<hr>
						@if($cashbond->cashbond_type != 'maintenance')
								{{ Form::label('add_amount', 'Additional Amount') }}
								{{ Form::text('add_amount_show',number_format($cashbond->add_amount,0), array('class' => 'form-control', 'placeholder' => 'Add Amount*', 'required' => 'true', 'id' => 'add_amount_show', 'oninput' => 'cal_sparator();')) }}<br/>
								{{ Form::hidden('add_amount',$cashbond->add_amount, old('add_amount'), array('id' => 'add_amount')) }}

								{{ Form::label('add_comment', 'Additional Decription') }}
								{{ Form::text('add_comment', old('add_comment'), array('class' => 'form-control', 'placeholder' => 'Additional Description*', 'required' => 'true')) }}  <br>
						@endif

						@if($cashbond->cashbond_type == 'maintenance')
						<button type="button" data-toggle="modal" data-target="#modal_cashbondconfirm" class="btn btn-success pull-right" style="margin-right: 10px"><i class="fa fa-check"></i> Closing</button>
						@elseif(isset($cashbond_item_count))
						<button type="button" class="btn btn-success pull-right" style="margin-right: 10px" disabled="disabled"><i class="fa fa-check"></i> Closing</button>
						@elseif(!isset($cashbond_details))
						<button type="button" class="btn btn-success pull-right" style="margin-right: 10px" disabled="disabled"><i class="fa fa-check"></i> Closing</button>
						@else
						<button type="button" data-toggle="modal" data-target="#modal_cashbondconfirm" class="btn btn-success pull-right" style="margin-right: 10px"><i class="fa fa-check"></i> Closing</button>
						@endif
						{!! Form::close() !!}
					</div>
				</div>
				<hr>
			</div>
		</div>
	</div>
</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_cashbondclose">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this cashbond detail?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_cashbondconfirm">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Closing this cashbond?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit_confirm" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_cashbondclose').submit();
	});

	$('#btn_submit_confirm').on('click', function()
	{
		$('#form_cashbondconfirm').submit();
	});
</script>
<script>
	$(".delete").on("submit", function(){
		return confirm("Delete this nota?");
	});
</script>
<script>
  function cal_sparator() {
		var add_amount_show = document.getElementById('add_amount_show').value;
		var result = document.getElementById('add_amount');
		var rsaddamount = (add_amount_show);
		result.value = rsaddamount.replace(/,/g, "");
	}

	window.onload= function(){

		n2= document.getElementById('add_amount_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp;
			if(who.id==='add_amount')  temp= validDigits(who.value,0);
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}
		n2.onblur= function(){
			var
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}

	}
</script>
@stop