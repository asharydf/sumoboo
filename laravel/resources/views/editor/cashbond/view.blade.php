@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.cashbond.index') }}"><i class="fa fa-dollar"></i> Cashbond</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Cashbond Finalized
						</h2>
					</div>
					<hr> 
					<div class="col-md-6">
						<div class="x_content"> 
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								<table id="table_revenuex" class="table  dataTable rwd-table">
									<tbody>
										<tr> 
											<th width="30%">Start Date</th>
											<td align="left">{{date('d-M-Y', strtotime($cashbond->start_date))}}</td>  
										</tr>
										<tr> 
											<th width="30%">End Date</th>
											<td align="left">{{date('d-M-Y', strtotime($cashbond->end_date))}}</td>  
										</tr>
										<tr> 
											<th width="30%">Budget Request</th>
											<td align="left">{{number_format($cashbond->budget_request,0)}}</td>  
										</tr>
										<tr> 
											<th width="30%">Cashout</th>
											<td align="left">{{number_format($cashbond->cashout,0)}}</td>  
										</tr>
										<tr> 
											<th width="30%">Remainder</th>
											<td align="left">{{number_format($cashbond->budget_issued-$cashbond->cashout,0)}}</td>  
										</tr>
										<tr> 
											<th width="30%">Add Amount</th>
											<td align="left">{{number_format($cashbond->add_amount,0)}}</td>  
										</tr>
										<tr> 
											<th width="30%">Grand Total</th>
											<td align="left">{{number_format($cashbond->cashout + $cashbond->add_amount,0)}}</td>  
										</tr> 
										<tr> 
											<th width="30%">Comment</th>
											<td align="left">{{$cashbond->comment}}</td>  
										</tr>
									</tbody> 
								</table>
								<hr>
								@foreach($array_all AS $all)
								<div class="div_overflow">
									<table id="drinkTable" class="table table-bordered dataTable">
										<thead>
											<tr> 
												<th>Date</th>
												<th>Nota Number</th>
												<th colspan="2">Description</th> 
												<th>Amount</th> 
												<th>Attachment</th>  
											</tr>
										</thead>
										<tbody>
											
											<tr> 
												<td>{{$all["date"]}}</td>
												<td>{{$all["nota_number"]}}</td>
												<td colspan="2">{{$all["description"]}}</td>
												<td>{{number_format($all["amount"],0)}}</td> 

												<td> 
													<a target="_blank" href="{{Config::get('constants.path.uploads')}}/cashbond_detail/{{$all["cashbond_attachment"]}}"><i class="fa fa-download"></i>&nbsp;Download</a> 
												</td> 
											</tr>

											<tr>
												<td><u>Item</u></td>
												<td><u>UOM</u></td>
												<td><u>Qty</u></td>
												<td><u>Price</u></td>
												<td><u>Amount</u></td> 
												<td><u>Paid</u></td>  
											</tr>
											@foreach($all["detail"] AS $detail_item)
											<tr>
												<td>{{$detail_item["item_name"]}}</td>
												<td>{{$detail_item["uom"]}}</td>
												<td>{{$detail_item["quantity"]}}</td>
												<td>{{number_format($detail_item["price"],0)}}</td>
												<td>{{number_format($detail_item["total"],0)}}</td> 
												<td>{{number_format($detail_item["paid"],0)}}</td> 
											</tr> 
											@endforeach
										</tbody>
									</table> <br>
								</div>
								@endforeach
								
							</div>
						</div> 
						
						<a href="{{ URL::route('editor.cashbondbank.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a><br>
					</div>   

				</div>
				<hr>
			</div>
		</div>
	</div>
</div>
</section>

@stop 
