@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-dot-circle-o"></i> Item Central</a></li>
	</ol>
</section>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>



<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>



						</h2>
							<br>
							<form class="form-inline" method="post">
								{{ csrf_field() }}

								<div class="form-group"  >
									<select name="type" class="form-control" id="type">
										<option value="" >Choose type</option>
									   	@if($type == 'consumable')
											<option value="consumable" selected>Consumable</option>
                                            <option value="uti">Utilization</option>
										@elseif($type == 'uti')
                                       		 <option value="consumable">Consumable</option>
										     <option value="uti" selected>Utilization</option>
										@else
											<option value="consumable">Consumable</option>
										    <option value="uti" >Utilization</option>
										@endif

									</select>
								</div>





							 	<div class="form-group">
									<select name="bulan" class="form-control" id="type">
									  <option value="">Pilih Bulan</option>
									  @for($i = 1; $i < 13; $i++)
									  	@if($i == $bulan)
										   <option value="{{ $i }}" selected>{{ $i }}</option>
										@else
											<option value="{{ $i }}">{{ $i }}</option>
										@endif
									  @endfor




									</select>
								</div>
								<div class="form-group">
									<select name="tahun" class="form-control" id="type">
									  <option value="">Pilih Tahun</option>
									  @if($tahun == '2017')
									    <option value="2017" selected>2017</option>
									  @else
										 <option value="2017">2017</option>
									  @endif
									</select>
								</div>



								<button type="submit" class="btn btn-default">Filter</button>

							</form>
							<br>
						<div class="x_content">
							<table id="item_centralTablex" class="table dataTable rwd-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Date</th>
										<th>Vendor Name Name</th>
										<th>Total</th>

									</tr>
									</thead>
									<tbody>
									@if(isset($reports))
										@foreach ($reports as $report)
											<tr>

												<td>{{ $loop->iteration }}</td>
												<td>{{ $report->invoice_date }}</td>
												<td>{{ $report->vendor_name }}</td>
												<td>{{ number_format($report->grand_total,0) }}</td>


                                            </tr>
										@endforeach

									@endif

									</tbody>
									</table>
								</div>

							</div>
						</div>



					</div>
				</div>
				<div id="curve_chart" style="width: 900px; height: 500px"></div>
			</section>
		</section>


@stop

@section('scripts')
 @if(isset($graph))
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Total'],

          {!! $graph !!}

        ]);

        var options = {
          title: 'Item Graph',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
</script>
  @endif
@stop