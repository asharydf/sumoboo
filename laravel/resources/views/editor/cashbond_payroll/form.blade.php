@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.cashbond_payroll.index') }}"><i class="fa fa-dollar"></i> Cashbond Payroll List</a></li>
		<li class="active">
			@if(isset($cashbond_payroll))
			<i class="fa fa-pencil"></i> Edit
			@else
			<i class="fa fa-plus"></i> Create
			@endif
		</li>
	</ol>
</section>
@actionStart('cashbond_payroll', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond_payroll))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Cashbond Payroll List
						</h2>
					</div>
					<hr>
					<div class="col-md-12">
						@include('errors.error')
						@if(isset($cashbond_payroll))
						{!! Form::model($cashbond_payroll, array('route' => ['editor.cashbond_payroll.update', $cashbond_payroll->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_payroll'))!!}
						@else
						{!! Form::open(array('route' => 'editor.cashbond_payroll.store', 'class'=>'create', 'id' => 'form_payroll'))!!}
						@endif
						{{ csrf_field() }}
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								{{ Form::label('date', 'Date') }}
								{{ Form::text('date', old('date'), ['class' => 'form-control', 'id' => 'date']) }}
								<br>

								<table class="table table-hover table-striped table-bordered">
									<thead>
										<tr>
											<th width="5%">#</th>
											<th>Name</th>
											<th>Cash Bond</th>

											<th>Miscellaneous</th>
											<th>Remark</th>
											<th><i class="fa fa-checkbox"></i></th>
										</tr>
									</thead>
									<tbody>
										@foreach($employees as $key => $employee)
										<tr>
											<td>{{$key+1}}</td>
											<td>{{$employee->emp_full_name}}</td>
											<td>
												@if(isset($cashbond_payroll))

												{{ Form::text('cashbond_payroll_detail['.$employee->id.'][cashbond_show]', old($employee->id.'[cashbond_show]', $employee->slr_cashbond_show), ['class' => 'form-control', 'id' => 'cashbond_show_'.$employee->id, 'min' => '0', 'disabled' => 'true', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

												{{ Form::hidden('cashbond_payroll_detail['.$employee->id.'][cashbond]', old($employee->id.'[cashbond]', $employee->slr_cashbond), ['class' => 'form-control', 'id' => 'cashbond_'.$employee->id, 'min' => '0', 'disabled' => 'true']) }}

												@else

												{{ Form::text('cashbond_payroll_detail['.$employee->id.'][cashbond_show]', old($employee->id.'[cashbond_show]', $employee->slr_cashbond_show), ['class' => 'form-control', 'id' => 'cashbond_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

												{{ Form::hidden('cashbond_payroll_detail['.$employee->id.'][cashbond]', old($employee->id.'[cashbond]', $employee->slr_cashbond), ['class' => 'form-control', 'id' => 'cashbond_'.$employee->id, 'min' => '0']) }}
												@endif
											</td>

											<td>


												@if(isset($cashbond_payroll))

												{{ Form::text('cashbond_payroll_detail['.$employee->id.'][misc_show]', old($employee->id.'[misc_show]', 0), ['class' => 'form-control', 'id' => 'misc_show_'.$employee->id, 'min' => '0', 'disabled' => 'true', 'oninput' => 'cal2_sparator'.$employee->id.'();']) }}

												{{ Form::hidden('cashbond_payroll_detail['.$employee->id.'][misc]', old($employee->id.'[misc]', 0), ['class' => 'form-control', 'id' => 'misc_'.$employee->id, 'min' => '0', 'disabled' => 'true']) }}

												@else

												{{ Form::text('cashbond_payroll_detail['.$employee->id.'][misc_show]', old($employee->id.'[misc_show]', 0), ['class' => 'form-control', 'id' => 'misc_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal2_sparator'.$employee->id.'();']) }}

												{{ Form::hidden('cashbond_payroll_detail['.$employee->id.'][misc]', old($employee->id.'[misc]', 0), ['class' => 'form-control', 'id' => 'misc_'.$employee->id, 'min' => '0']) }}
												@endif
											</td>
											<td>
												@if(isset($cashbond_payroll))

												{{ Form::text('cashbond_payroll_detail['.$employee->id.'][remark]', old($employee->id.'[remark]', 0), ['class' => 'form-control', 'id' => 'remark_'.$employee->id, 'disabled' => 'true']) }}



												@else

												{{ Form::text('cashbond_payroll_detail['.$employee->id.'][remark]', old($employee->id.'[remark]', ''), ['class' => 'form-control', 'id' => 'remark_'.$employee->id]) }}


												@endif
											</td>
											<td>
												@if(isset($cashbond_payroll))
												{{ Form::checkbox('cashbond_payroll_detail['.$employee->id.'][checkbox]', $employee->id, null, ['id' => 'checkbox_'.$employee->id, 'class' => 'checkbox']) }}
												@else
												{{ Form::checkbox('cashbond_payroll_detail['.$employee->id.'][checkbox]', $employee->id, $employee->id, ['id' => 'checkbox_'.$employee->id, 'class' => 'checkbox']) }}
												@endif
											</td>

										</tr>

										<script>
											function cal_sparator{{$employee->id}}() {
												var cashbond_show_{{$employee->id}} = document.getElementById('cashbond_show_{{$employee->id}}').value;
												var result_{{$employee->id}} = document.getElementById('cashbond_{{$employee->id}}');
												var rscashbond_{{$employee->id}} = (cashbond_show_{{$employee->id}});
												result_{{$employee->id}}.value = rscashbond_{{$employee->id}}.replace(/,/g, "");

												n{{$employee->id}}= document.getElementById('cashbond_show_{{$employee->id}}');

												n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
													e=e|| window.event;
													var who=e.target || e.srcElement,temp{{$employee->id}};
													if(who.id==='cashbond_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
													else temp{{$employee->id}}= validDigits(who.value);
													who.value= addCommas(temp{{$employee->id}});
												}
												n{{$employee->id}}.onblur= function(){
													var
													temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
													if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
												}
											}

											function cal2_sparator{{$employee->id}}() {
												var misc_show_{{$employee->id}} = document.getElementById('misc_show_{{$employee->id}}').value;
												var result_{{$employee->id}} = document.getElementById('misc_{{$employee->id}}');
												var rsmisc_{{$employee->id}} = (misc_show_{{$employee->id}});
												result_{{$employee->id}}.value = rsmisc_{{$employee->id}}.replace(/,/g, "");

												n{{$employee->id}}= document.getElementById('misc_show_{{$employee->id}}');

												n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
													e=e|| window.event;
													var who=e.target || e.srcElement,temp{{$employee->id}};
													if(who.id==='misc_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
													else temp{{$employee->id}}= validDigits(who.value);
													who.value= addCommas(temp{{$employee->id}});
												}
												n{{$employee->id}}.onblur= function(){
													var
													temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
													if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
												}
											}

											function cal3_sparator{{$employee->id}}() {
												var remark_show_{{$employee->id}} = document.getElementById('remark_show_{{$employee->id}}').value;
												var result_{{$employee->id}} = document.getElementById('remark_{{$employee->id}}');
												var rsremark_{{$employee->id}} = (remark_show_{{$employee->id}});
												result_{{$employee->id}}.value = rsremark_{{$employee->id}}.replace(/,/g, "");

												n{{$employee->id}}= document.getElementById('remark_show_{{$employee->id}}');

												n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
													e=e|| window.event;
													var who=e.target || e.srcElement,temp{{$employee->id}};
													if(who.id==='remark_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
													else temp{{$employee->id}}= validDigits(who.value);
													who.value= addCommas(temp{{$employee->id}});
												}
												n{{$employee->id}}.onblur= function(){
													var
													temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
													if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
												}
											}
										</script>
										@endforeach
									</tbody>
								</table>

								<button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
							</div>
						</div>

						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_confirm">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="summary_cashbond_payroll_date"></h4>
			</div>
			<div class="modal-body">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Cashbond</th>
							<th>Miscellaneous</th>
							<th>Remark</th>
						</tr>
					</thead>
					<tbody id="summary_cashbond_payroll_detail">
						{{-- cashbond_payroll DETAIL --}}
					</tbody>
					<tfoot>
					<tr>
						<td colspan="2" align="right"><b>TOTAL</b></td>
						<td id="grand_total"></td>
						<td id="grand_total_misc"></td>
						<td id="grand_total_remark"></td>
					</tr>
					</tfoot>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_payroll').submit();
	});

	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	function checkbox_toggle(id)
	{
		if($('#checkbox_'+id).is(":checked"))
		{
			$('#cashbond_'+id).attr('disabled', false);
			$('#cashbond_show_'+id).attr('disabled', false);

			$('#misc_'+id).attr('disabled', false);
			$('#misc_show_'+id).attr('disabled', false);

			$('#remark_'+id).attr('disabled', false);
			$('#remark_show_'+id).attr('disabled', false);

		} else {
			$('#cashbond_'+id).val('');
			$('#cashbond_'+id).attr('disabled', true);
			$('#cashbond_show_'+id).val('');
			$('#cashbond_show_'+id).attr('disabled', true);

			$('#misc_'+id).val('');
			$('#misc_'+id).attr('disabled', true);
			$('#misc_show_'+id).val('');
			$('#misc_show_'+id).attr('disabled', true);


			$('#remark_'+id).val('');
			$('#remark_'+id).attr('disabled', true);
			$('#remark_show_'+id).val('');
			$('#remark_show_'+id).attr('disabled', true);
		}
	}

	$('.checkbox').on('change', function()
	{
		console.log("assad");
		checkbox_toggle($(this).val());
	});

	$('#btn_confirm').on('click', function()
	{
		$('#summary_cashbond_payroll_date').text('Date: '+$('#date').val());

		var new_row = '';
		@if(isset($employees))
		var i = 0;
		var grand_total = 0;
		var grand_total_misc = 0;
		var grand_total_remark = 0;
		jQuery.each({!! $employees !!}, function (key, value)
		{
			//var total = $('#cashbond_'+id).val();

			if($('#checkbox_'+value['id']).is(":checked"))
			{
				i++;

				var total = Math.round((parseFloat($('#cashbond_'+value['id']).val())));
				var total_misc = Math.round((parseFloat($('#misc_'+value['id']).val())));
				var total_remark = Math.round((parseFloat($('#remark_'+value['id']).val())));

				new_row += '<tr>';
				new_row += '<td>';
				new_row += i;
				new_row += '</td>';
				new_row += '<td>';
				new_row += value['emp_full_name'];
				new_row += '</td>';
				new_row += '<td>';
				new_row += numberWithCommas($('#cashbond_'+value['id']).val());
				new_row += '</td>';
				new_row += '<td>';
				new_row += numberWithCommas($('#misc_'+value['id']).val());
				new_row += '</td>';
				new_row += '<td>';
				new_row += $('#remark_'+value['id']).val();
				new_row += '</td>';
				new_row += '</tr>';

				grand_total += total;
				grand_total_misc += total_misc;
				// grand_total_remark += total_remark;
			}
		});

		$('#grand_total').html(numberWithCommas(grand_total));
		$('#grand_total_misc').html(numberWithCommas(grand_total_misc));
		//$('#grand_total_remark').html(numberWithCommas(grand_total_remark));

		$('#summary_cashbond_payroll_detail').empty().append(new_row);

		@endif

	});

</script>

@if(isset($cashbond_payroll))
<script>
	jQuery.each({!! $cashbond_payroll->cashbond_payroll_detail !!}, function(key, value)
	{
		$('#checkbox_'+value['employee_id']).attr('checked', true);

		checkbox_toggle(value['employee_id']);

		$('#cashbond_'+value['employee_id']).val(value['cashbond']);
		$('#misc_'+value['employee_id']).val(value['cashbond']);
		$('#remark_'+value['employee_id']).val(value['cashbond']);
	});
</script>
@endif

@stop
