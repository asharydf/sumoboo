@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.revenue.index') }}"><i class="fa fa-money"></i> Revenue</a></li>
    <li class="active"><i class="fa fa-search"></i> Detail</li>
  </ol>
</section>
@actionStart('revenue', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-search"></i>&nbsp;&nbsp;Revenue {{date('d F Y', strtotime($revenue->date))}}
	                	</h2>
		                <hr>
			           <div class="x_content">
			                <div class="col-md-6 col-xs-12 col-sm-12">
			                	<table class="table">
			                		<tr>
			                			<th width="40%">Date</th>
			                			<td width="60%">{{date("D, d M Y", strtotime($revenue->date))}}</td>
			                		</tr>
			                		<tr>
			                			<th>Cash</th>
			                			<td>{{number_format($revenue->cash, 2)}}</td>
			                		</tr>
			                		<tr>
			                			<th>Debit BCA</th>
			                			<td>{{number_format($revenue->debit_bca, 2)}}</td>
			                		</tr>
			                		<tr>
			                			<th>Mastercard</th>
			                			<td>{{number_format($revenue->mastercard, 2)}}</td>
			                		</tr>
			                		<tr>
			                			<th>Visacard</th>
			                			<td>{{number_format($revenue->visacard, 2)}}</td>
			                		</tr>
			                		<tr>
			                			<th>Flazz</th>
			                			<td>{{number_format($revenue->flazz, 2)}}</td>
			                		</tr>
			                		<tr>
			                			<th>Other</th>
			                			<td>{{number_format($revenue->other, 2)}}</td>
			                		</tr>
			                		@if($revenue->revenue_attachment)
			                		<tr>
			                			<th>Attachment</th>
			                			<td>
			                				{!! Form::open(['route' => ['editor.revenue.download', $revenue->id]]) !!}
			                					<button type="submit" class="btn btn-info btn-xs">
			                						<i class="fa fa-download"></i> Download
			                					</button>
			                				{!! Form::close() !!}
			                			</td>
			                		</tr>
			                		@endif

			                		<tr>
			                			<th>Transfer Receipt</th>
			                			<td>
			                				@if($revenue->attachment_tranfer == '')
			                					{!! Form::model($revenue, array('route' => ['editor.revenue.updatetransferreceipt', $revenue->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_revenue', 'files' => 'true'))!!}

					                				{{ Form::label('filename', 'Attachment (max 5 Mb)') }}
													{{ Form::file('filename') }}
													<br>
													<button type="button" id="btn_submit" data-toggle="modal" data-target="#modal_summary" class="btn btn-primary btn-lg"><i class="fa fa-upload"></i> Upload</button>

												{!! Form::close() !!}

											@else
 
				                					<a href="{{Config::get('constants.path.uploads')}}/revenue/updatetransferreceipt/{{$revenue->attachment_tranfer}}" class="btn btn-info btn-xs" target="_blank">
				                						<i class="fa fa-download"></i> Download
				                					</a>

			                				@endif
			                			</td>
			                		</tr>
			                	</table>
			                </div>
			                <div class="col-md-6 col-xs-12 col-sm-12">
			                	<table class="table">
			                		<tr>
			                			<th width="40%">Total</th>
			                			<td width="60%">{{number_format($revenue->total, 2)}}</td>
			                		</tr>
			                		<tr>
			                			<th>Service Charge</th>
			                			<td>{{number_format($revenue->service_charge, 2)}}</td>
			                		</tr>
			                		<tr>
			                			<th>Tax</th>
			                			<td>{{number_format($revenue->tax, 2)}}</td>
			                		</tr>
			                		<tr>
			                			<th>Net Revenue</th>
			                			<td>{{number_format($revenue->vat, 2)}}</td>
			                		</tr>
			                	</table>
			                </div>

			                <div class="col-md-12 col-xs-12 col-sm-12">
			                	@if($revenue->status == 0)
			                		@actionStart('revenue', 'submit')
			                		{!! Form::open(['route' => 'editor.revenue.close', 'method' => 'PUT', 'id' => 'form_close']) !!}
			                		<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
			                		<button type="button" class="btn btn-lg btn-default" id="btn_close"><i class="fa fa-lock"></i> Close</button>
			                		{!! Form::close() !!}
			                		@actionEnd
		                		@elseif($revenue->status == 1)
		                			@actionStart('revenue', 'update')
		                			{!! Form::open(['route' => 'editor.revenue.request_edit', 'method' => 'PUT', 'id' => 'form_request_edit']) !!}
			                		<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
		                			<button type="button" class="btn btn-lg btn-default" id="btn_request_edit"><i class="fa fa-pencil"></i> Request Edit</button>
	                				{!! Form::close() !!}
	                				@actionEnd
		                		@elseif($revenue->status == 2)
		                			@actionStart('revenue', 'paid')
		                			<table>
		                				<tr>
		                					<td>
		                						{!! Form::open(['route' => 'editor.revenue.finance_approve', 'method' => 'PUT', 'id' => 'form_finance_reject']) !!}
							                	<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
							                	<input type="hidden" name="review" value="0">
							                	<button type="button" class="btn btn-danger btn-lg" style="margin:5px" id="btn_finance_reject">
							                		<i class="fa fa-remove"></i> Reject
						                		</button>
						                		{!! Form::close() !!}
		                					</td>
		                					<td>
		                						{!! Form::open(['route' => 'editor.revenue.finance_approve', 'method' => 'PUT', 'id' => 'form_finance_approve']) !!}
							                	<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
							                	<input type="hidden" name="review" value="1">
							                	<button type="button" class="btn btn-success btn-lg" style="margin:5px" id="btn_finance_approve">
							                		<i class="fa fa-check"></i> Approve
						                		</button>
						                		{!! Form::close() !!}
		                					</td>
		                				</tr>
		                			</table>
		                			@actionEnd
		                		@elseif($revenue->status == 3)
		                			@actionStart('revenue', 'issued')
		                			<table>
		                				<tr>
		                					<td>
		                						{!! Form::open(['route' => 'editor.revenue.owner_approve', 'method' => 'PUT', 'id' => 'form_owner_reject']) !!}
							                	<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
							                	<input type="hidden" name="review" value="0">
							                	<button type="button" class="btn btn-danger btn-lg" style="margin:5px" id="btn_owner_reject">
							                		<i class="fa fa-remove"></i> Reject
						                		</button>
						                		{!! Form::close() !!}
		                					</td>
		                					<td>
		                						{!! Form::open(['route' => 'editor.revenue.owner_approve', 'method' => 'PUT', 'id' => 'form_owner_approve']) !!}
							                	<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
							                	<input type="hidden" name="review" value="1">
							                	<button type="button" class="btn btn-success btn-lg" style="margin:5px" id="btn_owner_approve">
							                		<i class="fa fa-check"></i> Approve
						                		</button>
						                		{!! Form::close() !!}
		                					</td>
		                				</tr>
		                			</table>
	                				@actionEnd
		                		@endif
	                		</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_confirmation">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title"><i class="fa fa-warning"></i>Confirmation</h5>
      		</div>
      		<div class="modal-body">
        		<div id="modal_confirm_body"></div>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-success" id="btn_confirm">OK</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      		</div>	
    	</div>
  	</div>
</div>
@stop

@section('modal')
<div class="modal fade" id="modal_summary">
  	<div class="modal-dialog modal-md">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Upload this transfer receipt?</h4>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
      			<button type="button" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> Yes</button>
      		</div>
    	</div>
  	</div>
</div>

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function () 
{
    $("#table_revenue").DataTable();
});

$('#btn_close').on('click', function()
{		
	$('#modal_confirm_body').text('Close revenue?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_close').submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('#btn_confirm').on('click', function()
{
	$('#form_revenue').submit();
});

$('#btn_request_edit').on('click', function()
{
	$('#modal_confirm_body').text('Request edit revenue?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_request_edit').submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('#btn_finance_reject').on('click', function()
{
	$('#modal_confirm_body').text('Reject edit revenue request?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_finance_reject').submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('#btn_finance_approve').on('click', function()
{
	$('#modal_confirm_body').text('Approve edit revenue request?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_finance_approve').submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('#btn_owner_reject').on('click', function()
{
	$('#modal_confirm_body').text('Reject edit revenue request?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_owner_reject').submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('#btn_owner_approve').on('click', function()
{
	$('#modal_confirm_body').text('Approve edit revenue request?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_owner_approve').submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});
</script> 
@stop