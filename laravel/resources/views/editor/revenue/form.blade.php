@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.revenue.index') }}"><i class="fa fa-money"></i> Revenue</a></li>
		<li class="active">
			@if(isset($revenue))
			<i class="fa fa-pencil"></i> Edit
			@else
			<i class="fa fa-plus"></i> Create
			@endif
		</li>
	</ol>
</section>
@actionStart('revenue', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($revenue))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Revenue
						</h2>
					</div>
					<hr>
					<div class="col-md-12">
					@include('errors.error')
					@if(isset($revenue))
					{!! Form::model($revenue, array('route' => ['editor.revenue.update', $revenue->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_revenue', 'files' => 'true'))!!}
					@else
					{!! Form::open(array('route' => 'editor.revenue.store', 'class'=>'create', 'id' => 'form_revenue', 'files' => 'true'))!!}
					@endif
					{{ csrf_field() }}
						<div class="x_content"> 
							<div class="row">
								<div class="col-md-6 col-sm-12 col-xs-12 form-group">
									{{ Form::label('date', 'Date') }}
									@if(isset($revenue))
									{{ Form::text('date', old('date'), ['class' => 'form-control', 'id' => 'date', 'disabled' => 'true']) }}
									{{ Form::hidden('date', old('date')) }}
									@else
									{{ Form::text('date', old('date', date('Y-m-d')), ['class' => 'form-control', 'id' => 'date']) }}
									@endif
									<br>

									{{ Form::label('cash', 'Cash') }}  
									@if(isset($revenue)) 
									{{ Form::text('cash_show',number_format($revenue->cash,0), array('class' => 'form-control', 'placeholder' => 'Cash*', 'required' => 'true', 'id' => 'cash_show', 'oninput' => 'cal_sparator();')) }} 
									@else
									{{ Form::text('cash_show',old('cash_show'), array('class' => 'form-control', 'placeholder' => 'Cash*', 'required' => 'true', 'id' => 'cash_show', 'oninput' => 'cal_sparator();')) }} 
									@endif 
									{{ Form::hidden('cash', old('cash'), array('id' => 'cash', 'min' => '0', 'step' => '1')) }}
									<br>

									{{ Form::label('debit_bca', 'Debit BCA') }}  
									@if(isset($revenue)) 
									{{ Form::text('debit_bca_show',number_format($revenue->debit_bca,0), array('class' => 'form-control', 'placeholder' => 'Debit BCA*', 'required' => 'true', 'id' => 'debit_bca_show', 'oninput' => 'cal_sparator();')) }} 
									@else
									{{ Form::text('debit_bca_show',old('debit_bca_show'), array('class' => 'form-control', 'placeholder' => 'Debit BCA*', 'required' => 'true', 'id' => 'debit_bca_show', 'oninput' => 'cal_sparator();')) }} 
									@endif 
									{{ Form::hidden('debit_bca', old('debit_bca'), array('id' => 'debit_bca', 'min' => '0', 'step' => '1')) }} 
									<br>

									{{ Form::label('mastercard', 'Mastercard') }}  
									@if(isset($revenue)) 
									{{ Form::text('debit_bca_show',number_format($revenue->mastercard,0), array('class' => 'form-control', 'placeholder' => 'Mastercard*', 'required' => 'true', 'id' => 'mastercard_show', 'oninput' => 'cal_sparator();')) }} 
									@else
									{{ Form::text('mastercard_show',old('mastercard_show'), array('class' => 'form-control', 'placeholder' => 'Mastercard*', 'required' => 'true', 'id' => 'mastercard_show', 'oninput' => 'cal_sparator();')) }} 
									@endif 
									{{ Form::hidden('mastercard', old('mastercard'), array('id' => 'mastercard', 'min' => '0', 'step' => '1')) }}
									<br>

									{{ Form::label('visacard', 'Visacard') }}  
									@if(isset($revenue)) 
									{{ Form::text('visacard_show',number_format($revenue->visacard,0), array('class' => 'form-control', 'placeholder' => 'Visacard*', 'required' => 'true', 'id' => 'visacard_show', 'oninput' => 'cal_sparator();')) }} 
									@else
									{{ Form::text('visacard_show',old('visacard_show'), array('class' => 'form-control', 'placeholder' => 'Visacard*', 'required' => 'true', 'id' => 'visacard_show', 'oninput' => 'cal_sparator();')) }} 
									@endif 
									{{ Form::hidden('visacard', old('visacard'), array('id' => 'visacard', 'min' => '0', 'step' => '1')) }}
									<br>

									{{ Form::label('flazz', 'Flazz') }}  
									@if(isset($revenue)) 
									{{ Form::text('flazz_show',number_format($revenue->flazz,0), array('class' => 'form-control', 'placeholder' => 'Flazz*', 'required' => 'true', 'id' => 'flazz_show', 'oninput' => 'cal_sparator();')) }} 
									@else
									{{ Form::text('flazz_show',old('flazz_show'), array('class' => 'form-control', 'placeholder' => 'Flazz*', 'required' => 'true', 'id' => 'flazz_show', 'oninput' => 'cal_sparator();')) }} 
									@endif 
									{{ Form::hidden('flazz', old('flazz'), array('id' => 'flazz', 'min' => '0', 'step' => '1')) }}
									<br>

									{{ Form::label('other', 'Other') }} 
									@if(isset($revenue)) 
									{{ Form::text('other_show',number_format($revenue->other,0), array('class' => 'form-control', 'placeholder' => 'Other*', 'required' => 'true', 'id' => 'other_show', 'oninput' => 'cal_sparator();')) }} 
									@else
									{{ Form::text('other_show',old('other_show'), array('class' => 'form-control', 'placeholder' => 'Other*', 'required' => 'true', 'id' => 'other_show', 'oninput' => 'cal_sparator();')) }} 
									@endif 
									{{ Form::hidden('other', old('other'), array('id' => 'other', 'min' => '0', 'step' => '1')) }}
									<br>

									{{ Form::label('filename', 'Attachment (max 5 Mb)') }}
									{{ Form::file('filename') }}
									<br>

									{{ Form::hidden('total', old('total'), ['id' => 'total']) }}
									{{ Form::hidden('tax', old('tax'), ['id' => 'tax']) }}
									{{ Form::hidden('service_charge', old('service_charge'), ['id' => 'service_charge']) }}
									{{ Form::hidden('vat', old('vat'), ['id' => 'vat']) }}
								</div>

								<div class="col-md-6 col-sm-12 col-xs-12 form-group">
									{{ Form::label('tax_rate', 'Tax Rate') }}
									<div class="input-group">
										{{ Form::number('tax_rate', old('tax_rate', 10), ['class' => 'form-control', 'step' => '0.01', 'id' => 'tax_rate']) }}
										<span class="input-group-addon"><i class="fa fa-percent"></i></span>
									</div>
									<br>

									{{ Form::label('service_charge_rate', 'Service Charge Rate') }}
									<div class="input-group">
										{{ Form::number('service_charge_rate', old('service_charge_rate', 7), ['class' => 'form-control', 'step' => '0.01', 'id' => 'service_charge_rate']) }}
										<span class="input-group-addon"><i class="fa fa-percent"></i></span>
									</div>
									<br>
								</div>
							</div>

							<button type="button" id="btn_submit" data-toggle="modal" data-target="#modal_summary" class="btn btn-primary btn-lg"><i class="fa fa-paper-plane"></i> Submit</button>
						</div>


					{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_summary">
  	<div class="modal-dialog modal-md">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Revenue Summary</h4>
      		</div>
      		<div class="modal-body">
      			<table class="table">
  				<tr>
  					<th width="25%">Total</th>
  					<td width="75%" id="summary_revenue_total"></td>
  				</tr>
  				<tr>
  					<th width="25%">Tax</th>
  					<td width="75%" id="summary_revenue_tax"></td>
  				</tr>
  				<tr>
  					<th width="25%">Service Charge</th>
  					<td width="75%" id="summary_revenue_service_charge"></td>
  				</tr>
  				<tr>
  					<th width="25%">Net Revenue</th>
  					<td width="75%" id="summary_revenue_vat"></td>
  				</tr>
				</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
      			<button type="button" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> Confirm</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
$('#btn_confirm').on('click', function()
{
	$('#form_revenue').submit();
});

function calculate()
{
	var total = 0;
	var service_charge_rate = $('#service_charge_rate').val();
	var service_charge = 0;
	var tax_rate = $('#tax_rate').val();
	var tax = 0;
	var vat = 0;

	total = parseInt($('#cash').val()) + parseInt($('#debit_bca').val()) + parseInt($('#mastercard').val()) + parseInt($('#visacard').val()) + parseInt($('#flazz').val()) + parseInt($('#other').val());
	// console.log(parseFloat(tax_rate));
	// console.log(parseFloat(total)); 
	tax = (parseFloat(total) / ( 100 + parseFloat(tax_rate))) * parseFloat(tax_rate);
 

	service_charge = (parseFloat(total) - parseFloat(tax)) / (100 + parseFloat(service_charge_rate)) * parseFloat(service_charge_rate);
	vat = parseFloat(total) - parseFloat(service_charge) - parseFloat(tax);

	$('#summary_revenue_total').text(total.toFixed(2));
	$('#summary_revenue_tax').text(tax.toFixed(2));
	$('#summary_revenue_service_charge').text(service_charge.toFixed(2));
	$('#summary_revenue_vat').text(vat.toFixed(2));

	$('#total').val(total.toFixed(2));
	$('#tax').val(tax.toFixed(2));
	$('#service_charge').val(service_charge.toFixed(2));
	$('#vat').val(vat.toFixed(2));
}

$('#btn_submit').on('click', function()
{
	calculate();
});

function cal_sparator() 
	{
		var debit_bca_show = document.getElementById('debit_bca_show').value;
		var result = document.getElementById('debit_bca');
		var rsdebit_bca = (debit_bca_show);
		result.value = rsdebit_bca.replace(/,/g, ""); 

		var mastercard_show = document.getElementById('mastercard_show').value;
		var result = document.getElementById('mastercard');
		var rsmastercard = (mastercard_show);
		result.value = rsmastercard.replace(/,/g, "");

		var visacard_show = document.getElementById('visacard_show').value;
		var result = document.getElementById('visacard');
		var rsvisacard = (visacard_show);
		result.value = rsvisacard.replace(/,/g, "");

		var flazz_show = document.getElementById('flazz_show').value;
		var result = document.getElementById('flazz');
		var rsflazz = (flazz_show);
		result.value = rsflazz.replace(/,/g, "");

		var other_show = document.getElementById('other_show').value;
		var result = document.getElementById('other');
		var rsother = (other_show);
		result.value = rsother.replace(/,/g, "");

		var cash_show = document.getElementById('cash_show').value;
		var result = document.getElementById('cash');
		var rscash = (cash_show);
		result.value = rscash.replace(/,/g, "");
	}

	window.onload= function(){ 
		
		n2= document.getElementById('debit_bca_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='debit_bca')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n2.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}

		n3= document.getElementById('mastercard_show');

		n3.onkeyup=n3.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='mastercard')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n3.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n3.value));
			if(temp2)n3.value=addCommas(temp2.toFixed(0));
		}

		n4= document.getElementById('visacard_show');

		n4.onkeyup=n4.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='visacard')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n4.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n4.value));
			if(temp2)n4.value=addCommas(temp2.toFixed(0));
		}

		n5= document.getElementById('flazz_show');

		n5.onkeyup=n5.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='flazz')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n5.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n5.value));
			if(temp2)n5.value=addCommas(temp2.toFixed(0));
		}

		n6= document.getElementById('other_show');

		n6.onkeyup=n6.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='other')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n6.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n6.value));
			if(temp2)n6.value=addCommas(temp2.toFixed(0));
		}

		n7= document.getElementById('cash_show');

		n7.onkeyup=n7.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='cash')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n7.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n7.value));
			if(temp2)n7.value=addCommas(temp2.toFixed(0));
		}

	}
</script>
@stop
