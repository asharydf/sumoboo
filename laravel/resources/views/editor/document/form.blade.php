@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.document.index') }}"><i class="fa fa-cog"></i> Document</a></li>
		<li class="active">


			@if(isset($document))
				<i class="fa fa-pencil"></i> Edit
					@else
				<i class="fa fa-plus"></i> Create
			@endif
		</li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($document))
								<i class="fa fa-pencil"></i> Edit Document
									@else
								<i class="fa fa-plus"></i> Document
							@endif


						</h2>
					</div>
					<hr>
					<div class="col-md-12">
					@include('errors.error')
					@if(isset($document))
					{!! Form::model($document, array('route' => ['editor.document.update', $document->id], 'files' => 'true', 'method' => 'PUT', 'class'=>'update', 'id' => 'form_payroll'))!!}
					@else
					{!! Form::open(array('route' => 'editor.document.store', 'files' => 'true', 'class'=>'create', 'id' => 'form_payroll'))!!}
					@endif
					{{ csrf_field() }}
						<div class="x_content">
						@if(isset($main_list))


							{{ Form::label('title', 'Title') }}
							{{ Form::text('title', old('title'), array('class' => 'form-control', 'placeholder' => 'Title', 'required' => 'true')) }}<br/>

							{{ Form::label('description', 'Description') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description', 'required' => 'true')) }}<br/>





							<br>


						@else



							{{ Form::label('title', 'Title') }}
							{{ Form::text('title', old('title'), array('class' => 'form-control', 'placeholder' => 'Title', 'required' => 'true')) }}<br/>

							{{ Form::label('description', 'Description') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description', 'required' => 'true')) }}<br/>





							<br>



						@endif
						<br>
							<button type="submit" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> Save</button>

						</div>

					{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop


@section('scripts')
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>


@stop
