<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_name' => 'required',
            'vendor_phone' => 'required',
            'vendor_address' => 'required',
            'vendor_email' => 'required',
            'vendor_bank' => 'required',
            'vendor_rekening' => 'required', 
        ];
    }

    public function messages()
    {
        return [
            'vendor_name.required' => 'Name is required',
            'vendor_phone.required' => 'Phone is required',
            'vendor_address.required' => 'Address is required',
            'vendor_email.required' => 'Email is required',
            'vendor_bank.required' => 'Bank is required',
            'vendor_rekening.required' => 'Rek no is required',
        ];
    }
}
