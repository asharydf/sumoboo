<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'emp_name' => 'required',
            'emp_full_name' => 'required',
            'emp_bod' => 'required',
            'emp_address' => 'required',
            'emp_ktp_number' => 'required',
            'emp_bank' => 'required',
            'emp_bank_rek' => 'required',
            'emp_email' => 'required',
            'emp_phone' => 'required',
            'emp_gender' => 'required',
            'emp_religion' => 'required',
            'emp_hiring_date' => 'required',
            'emp_position' => 'required',
            'slr_basic' => 'required',
            'slr_voucher' => 'required',
            'slr_transport' => 'required',
            'slr_tunjangan' => 'required',
            'slr_tunjangan_makan' => 'required',
            'slr_cashbond' => 'required',
            'slr_thr' => 'required',
            'slr_pot_deposit' => 'required' 
        ];
    }

    public function messages()
    {
        return [
            'emp_name.required' => 'Name is required',
            'emp_full_name.required' => 'Full name is required',
            'emp_bod.required' => 'BOD is required',
            'emp_address.required' => 'Address is required',
            'emp_ktp_number.required' => 'KTP number is required',
            'emp_bank.required' => 'Bank is required',
            'emp_bank_rek.required' => 'Rek bank is required',
            'emp_email.required' => 'Email is required',
            'emp_phone.required' => 'Phone is required',
            'emp_gender.required' => 'Gender is required',
            'emp_religion.required' => 'Religion is required',
            'emp_hiring_date.required' => 'Hiring date is required',
            'emp_position.required' => 'Position is required',
            'slr_basic.required' => 'Salary basic is required',
            'slr_voucher.required' => 'Voucher is required',
            'slr_transport.required' => 'Transport is required',
            'slr_tunjangan.required' => 'Tunjangan is required',
            'slr_tunjangan_makan.required' => 'Tunjangan makan is required',
            'slr_cashbond.required' => 'Cashbond is required',
            'slr_thr.required' => 'THR is required',
            'slr_pot_deposit.required' => 'Deposit is required',
        ];
    }
}
