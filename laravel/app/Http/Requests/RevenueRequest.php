<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RevenueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date|unique:revenue,branch_id,NULL,id,date,'.$this->id,
            'cash' => 'required|numeric',
            'debit_bca' => 'required|numeric',
            'mastercard' => 'required|numeric',
            'visacard' => 'required|numeric',
            'flazz' => 'required|numeric',
            'other' => 'required|numeric',
            'total' => 'required|numeric',
            'service_charge' => 'required|numeric',
            'tax' => 'required|numeric',
            'vat' => 'required|numeric',
            'filename' => 'max:5000',
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Date is required.',
            'date.date' => 'Date must be a valid date format.', 
            'date.unique' => 'Revenue for that date already exists.',

            'cash.required' => 'Cash is required.', 
            'cash.numeric' => 'Cash must be a valid number.', 

            'debit_bca.required' => 'Debit BCA is required.', 
            'debit_bca.numeric' => 'Debit BCA must be a valid number.', 

            'mastercard.required' => 'Mastercard is required.', 
            'mastercard.numeric' => 'Mastercard must be a valid number.', 

            'visacard.required' => 'Visacard is required.', 
            'visacard.numeric' => 'Visacard must be a valid number.', 

            'flazz.required' => 'Flazz is required.', 
            'flazz.numeric' => 'Flazz must be a valid number.', 

            'other.required' => 'Other is required.', 
            'other.numeric' => 'Other must be a valid number.', 

            'total.required' => 'Total is required.', 
            'total.numeric' => 'Total must be a valid number.', 

            'service_charge.required' => 'Service Charge is required.', 
            'service_charge.numeric' => 'Service Charge must be a valid number.', 

            'tax.required' => 'Tax is required.', 
            'tax.numeric' => 'Tax must be a valid number.', 

            'vat.required' => 'VAT is required.', 
            'vat.numeric' => 'VAT must be a valid number.', 

            'filename.max' => 'File max size is 5 Mb', 
        ];
    }
}
