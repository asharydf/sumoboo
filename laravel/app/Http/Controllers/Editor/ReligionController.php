<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\ReligionRequest;
use App\Http\Controllers\Controller;
use App\Model\Religion;

class ReligionController extends Controller
{
    public function index()
    {
    	$religions = Religion::all();
    	return view ('editor.religion.index', compact('religions'));
    }

    public function create()
    { 
        return view ('editor.religion.form');

    }

    public function store(ReligionRequest $request)
    {
    	$religion = new Religion;
    	$religion->religion_name = $request->input('religion_name');
    	$religion->religion_desc = $request->input('religion_desc');
        $religion->created_by = Auth::id();
    	$religion->save();

    	return redirect()->action('Editor\ReligionController@index');
    }

    public function edit($id)
    {
    	$religion = Religion::Find($id); 
    	return view ('editor.religion.form', compact('religion'));
    }

    public function update($id, ReligionRequest $request)
    {
    	$religion = Religion::Find($id);
        $religion->religion_name = $request->input('religion_name');
    	$religion->religion_desc = $request->input('religion_desc');
        $religion->updated_by = Auth::id();
    	$religion->save();

    	return redirect()->action('Editor\ReligionController@index');
    }

    public function delete($id)
    {
    	Religion::Find($id)->delete();
    	return redirect()->action('Editor\ReligionController@index');
    }
}
