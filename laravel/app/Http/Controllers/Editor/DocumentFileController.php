<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\DocumentFile;
use Illuminate\Support\Facades\Storage;

class DocumentFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
       $lists = DocumentFile::where('document_id',$id)->get();
       return view('editor.document_file.index', compact('lists','id'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('editor.document_file.form',compact('lists','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'file' => 'required',
        ]);

        $document = new DocumentFile();
        $document->document_id = $id;
        $document->title = $request->title;
        $document->description = $request->description;
        $document->created_by = Auth::id();

        //Store Image and create Thumbnail
        if($request->file('file') )
        {
            $file_dir = "uploads/document/";
            if(!File::exists($file_dir))
            {
                File::makeDirectory($file_dir, $mode = 0777, true, true);
            }


            $file = $request->file('file')->getClientOriginalName();



            $document->file = date('h-i-s')." ".$file;



            $request->file('file')->move($file_dir, $document->file);



        }


        $document->save();

       // return "sukses";
        return redirect()->route('editor.document_file.index',$id);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = DocumentFile::find($id);
        return view('editor.document_file.form', compact('document','id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',


        ]);

        $document = DocumentFile::find($id);
        $document->title = $request->title;
        $document->description = $request->description;

        $document->updated_by = Auth::id();

        //Store Image and create Thumbnail
        if($request->file('file') )
        {
            $file_dir = "uploads/document/";
            if(!File::exists($file_dir))
            {
                File::makeDirectory($file_dir, $mode = 0777, true, true);
            }


            $file = $request->file('file')->getClientOriginalName();
            $document->file = date('h-i-s')." ".$file;
            $request->file('file')->move($file_dir, $document->file);



        }
        $id_doc = $document->document_id;

        $document->save();

        return redirect()->route('editor.document_file.index',$id_doc);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $document = DocumentFile::find($id);
        $file = "uploads/document/".$document->file;
        $document->deleted_by = Auth::id();
        $document->save();

        File::delete($file);

        $document->delete();
        return back();
    }



}
