<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Document;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $lists = Document::paginate(15);
       return view('editor.document.index', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('editor.document.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',

        ]);

        $document = new Document();
        $document->title = $request->title;
        $document->description = $request->description;
        $document->created_by = Auth::id();

        $document->save();

        return redirect()->route('editor.document.index');

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::find($id);
        return view('editor.document.form', compact('document'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
        ]);

        $document = Document::find($id);
        $document->title = $request->title;
        $document->description = $request->description;
        $document->updated_by = Auth::id();


        $document->save();
        return redirect()->route('editor.document.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $document = Document::find($id);

        $document->deleted_by = Auth::id();
        $document->save();



        $document->delete();
        return back();
    }



}
