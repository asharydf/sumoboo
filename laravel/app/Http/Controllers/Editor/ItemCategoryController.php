<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ItemCategoryRequest;
use App\Http\Controllers\Controller;
use App\Model\ItemCategory;
use App\Model\ItemType;

class ItemCategoryController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $item_categorys = ItemCategory::paginate(15);
    	return view ('editor.item_category.index', compact('item_categorys'))->with('number',$no);
    }

    public function create()
    {
    	$item_type_list = ItemType::all()->pluck('type_name', 'id'); 
        return view ('editor.item_category.form', ['item_type_list' => $item_type_list]);

    }

    public function store(ItemCategoryRequest $request)
    {
    	$item_category = new ItemCategory;
    	$item_category->category_name = $request->input('category_name');
    	$item_category->category_desc = $request->input('category_desc');
        $item_category->item_type_id = $request->input('item_type_id');
        $item_category->created_by = Auth::id();
    	$item_category->save();

    	return redirect()->action('Editor\ItemCategoryController@index');
    }

    public function edit($id)
    {
    	$item_category = ItemCategory::Find($id); 
        $item_type_list = ItemType::all()->pluck('type_name', 'id'); 
    	return view ('editor.item_category.form',  compact('item_category', 'item_type_list'));
    }

    public function update($id, ItemCategoryRequest $request)
    {
    	$item_category = ItemCategory::Find($id);
        $item_category->category_name = $request->input('category_name');
    	$item_category->category_desc = $request->input('category_desc');
        $item_category->item_type_id = $request->input('item_type_id');
        $item_category->updated_by = Auth::id();
    	$item_category->save();

    	return redirect()->action('Editor\ItemCategoryController@index');
    }

    public function delete($id)
    {
    	ItemCategory::Find($id)->delete();
    	return redirect()->action('Editor\ItemCategoryController@index');
    }
}
