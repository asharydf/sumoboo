<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\ActionRequest;
use App\Http\Controllers\Controller;
use App\Model\Action;

class ActionController extends Controller
{
    public function index()
    {
    	$actions = Action::paginate(15);
    	return view ('editor.action.index', compact('actions'));
    }

    public function create()
    {
    	return view ('editor.action.form');
    }

    public function store(ActionRequest $request)
    {
    	$action = new Action;
    	$action->name = $request->input('name');
    	$action->description = $request->input('description');
    	$action->save();

    	return redirect()->action('Editor\ActionController@index');
    }

    public function edit($id)
    {
    	$action = Action::find($id);
    	return view ('editor.action.form', compact('action'));
    }

    public function update($id, Request $request)
    {
    	$action = Action::find($id);
    	$action->description = $request->input('description');
    	$action->save();
        return redirect()->action('Editor\ActionController@index');    
    }

    public function delete($id)
    {
    	Action::find($id)->delete();
        return redirect()->action('Editor\ActionController@index');
    }
}
