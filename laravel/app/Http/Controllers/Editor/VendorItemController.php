<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\VendorItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Vendor;
use App\Model\InvoiceType;
use App\Model\Item;
use App\Model\VendorItem;

class VendorItemController extends Controller
{
    public function index()
    {
        if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14;
        $vendors =  DB::table('vendor')
        ->leftjoin('invoice_type', 'vendor.invoice_type_id', '=', 'invoice_type.id')
        ->select('vendor.id',
            'invoice_type.inv_type_name',
            'vendor.vendor_name',
            'vendor.vendor_phone',
            'vendor.vendor_address',
            'vendor.vendor_email',
            'vendor.vendor_bank',
            'vendor.vendor_rekening',
            'vendor.vendor_pic',
            'vendor.vendor_pic_num') 
        ->whereNull('vendor.deleted_at')
        ->orderBy('vendor.vendor_name', 'ASC')
        ->paginate(15); 

        //dd($vendors);

        return view ('editor.vendoritem.index', compact('vendors'))->with('number',$no);
    }

    public function edit($id)
    {
        $vendor = Vendor::Find($id); 
        $item_list = Item::pluck('item_name', 'id'); 
        $vendor_item =  DB::table('vendor_item') 
        ->leftjoin('item', 'item.id', '=', 'vendor_item.item_id')
        ->select('item.item_name', 
            'item.item_desc', 
            'vendor_item.id') 
        ->whereNull('vendor_item.deleted_at') 
        ->where('vendor_item.vendor_id', $id)->get();

        //dd($vendor_item);

        return view ('editor.vendoritem.form', compact('vendor', 'item_list', 'vendor_item'));
    } 

    public function store(VendorItemRequest $request, $id)
    {
        $vendoritem = new Vendoritem;
        $vendoritem->item_id = $request->input('item_id');
        $vendoritem->vendor_id = $id; 
        $vendoritem->created_by = Auth::id();
        $vendoritem->save();
 
        return redirect()->action('Editor\VendorItemController@edit', $id);
    }

    public function delete($id)
    {
        VendorItem::Find($id)->delete();
        //return redirect()->action('Editor\VendorItemController@edit', $id);
        return redirect()->back();
    }
}
