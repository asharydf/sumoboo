<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\InvoiceTypeRequest;
use App\Http\Controllers\Controller;
use App\Model\InvoiceType;
use App\Model\ItemCategory;

class InvoiceTypeController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $invoice_types = InvoiceType::paginate(15);
    	return view ('editor.invoice_type.index', compact('invoice_types'))->with('number',$no);
    }

    public function create()
    { 
        $item_cat_list = ItemCategory::where('id', 1)->orwhere('id', 4)->pluck('category_name', 'id');
        return view ('editor.invoice_type.form', ['item_cat_list' => $item_cat_list]);

    }

    public function store(InvoiceTypeRequest $request)
    {
    	$invoice_type = new InvoiceType;
        $invoice_type->item_category_id = $request->input('item_category_id');
    	$invoice_type->inv_type_name = $request->input('inv_type_name');
    	$invoice_type->inv_type_desc = $request->input('inv_type_desc');
        $invoice_type->created_by = Auth::id();
    	$invoice_type->save();

    	return redirect()->action('Editor\InvoiceTypeController@index');
    }

    public function edit($id)
    {
    	$invoice_type = InvoiceType::Find($id);
        $item_cat_list = ItemCategory::where('id', 1)->orwhere('id', 4)->pluck('category_name', 'id');
    	return view ('editor.invoice_type.form', ['item_cat_list' => $item_cat_list], compact('invoice_type'));
    }

    public function update($id, InvoiceTypeRequest $request)
    {
    	$invoice_type = InvoiceType::Find($id);
        $invoice_type->item_category_id = $request->input('item_category_id');
        $invoice_type->inv_type_name = $request->input('inv_type_name');
    	$invoice_type->inv_type_desc = $request->input('inv_type_desc');
        $invoice_type->updated_by = Auth::id();
    	$invoice_type->save();

    	return redirect()->action('Editor\InvoiceTypeController@index');
    }

    public function delete($id)
    {
    	InvoiceType::Find($id)->delete();
    	return redirect()->action('Editor\InvoiceTypeController@index');
    }
}
