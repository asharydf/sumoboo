<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\Model\ItemCategory;

class ItemController extends Controller
{
    public function index($id)
    {
        if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 

        //$items = Item::all();
        $id_item_type = $id;
        $items = DB::table('item')
        ->leftjoin('item_category', 'item.item_category_id', '=', 'item_category.id') 
        ->select('item.id',
            'item.item_name',
            'item.item_desc',
            'item.item_category_id',
            'item.price',
            'item.uom',
            'item_category.category_name',
            'item_category.item_type_id') 
        ->where('item_category.item_type_id', $id)
        ->whereNull('item.deleted_at')
        ->paginate(15);
 
    	return view ('editor.item.index', compact('items', 'id_item_type'))->with('number',$no);
    }

    public function create($id)
    {
        $id_item_type = $id;
    	$item_cat_list = ItemCategory::where('item_type_id', $id)->pluck('category_name', 'id');

       //dd($item_cat_list);

        return view ('editor.item.form', ['item_cat_list' => $item_cat_list], compact('id_item_type'));
    }

    public function store($id, ItemRequest $request)
    {
    	$item = new Item;
    	$item->item_name = $request->input('item_name');
    	$item->item_desc = $request->input('item_desc');
        $item->item_category_id = $request->input('item_category_id');
        $item->uom = $request->input('uom');
        $item->price = $request->input('price');
        $item->created_by = Auth::id();
    	$item->save();

    	//return redirect()->action('Editor\ItemController@index');
        return redirect('editor/item/'.$id.'');
    }

    public function edit($id, $id2)
    {
    	$id_item_type = $id;
        //$item = Item::Find($id); 

         $item = DB::table('item')
        ->leftjoin('item_category', 'item.item_category_id', '=', 'item_category.id') 
        ->select('item.id',
            'item.item_name',
            'item.item_desc',
            'item.item_category_id',
            'item.price',
            'item.uom',
            'item_category.category_name',
            'item_category.item_type_id') 
        ->where('item.id', $id2)
        ->whereNull('item.deleted_at')
        ->first();

        //dd($item);

        $item_cat_list = ItemCategory::where('item_type_id', $id)->pluck('category_name', 'id');
    	return view ('editor.item.form', compact('item', 'item_cat_list', 'id_item_type'));
    }

    public function update($id, $id2, ItemRequest $request)
    {
    	$item = Item::Find($id2);
        $item->item_name = $request->input('item_name');
    	$item->item_desc = $request->input('item_desc');
        $item->item_category_id = $request->input('item_category_id');
        $item->uom = $request->input('uom');
        $item->price = $request->input('price');
        $item->updated_by = Auth::id();
    	$item->save();

    	//return redirect()->action('Editor\ItemController@index');
        return redirect('editor/item/'.$id.'');
    }

    public function delete($id, $id2)
    {
    	Item::Find($id2)->delete();
    	//return redirect()->action('Editor\ItemController@index');
        return redirect('editor/item/'.$id.'');
    }
}
