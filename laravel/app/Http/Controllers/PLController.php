<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class PLController extends Controller
{
    public function index(Request $request)
    {




        if ($request->start_date && $request->end_date) {
            $data[] = "";
            $data['revenue']  = 0;
            $data['air'] = 0;
            $data['cash_bahan_baku'] = 0;
            $data['kost'] = 0;
            $data['listrik'] = 0;
            $data['salary'] = 0;
            $data['ipl'] = 0;
            $data['satpam'] = 0;
            $data['internet'] = 0;
            $data['suplay_sumobo'] = 0;
            $data['cons_direct_vendor'] = 0;
            $data['cons_supp'] = 0;
            $data['franchise_amortization'] = 0;
            $data['cash_operational'] = 0;
            $data['asset_deprecation'] = 0;
            $data['rent_ruko'] = 0;
            $data['franchise_fee'] = 0;
            $data['catring'] = 0;
            $data['cash_op'] = 0;
            $data['solar'] = 0;
            $data['misc'] = 0;
            $data['total_o_p'] = 0;
            $data['tax_royalti'] = 0;
            $data['cogs'] = 0;
            $data['gross_profit'] =  0;
            $data['net_before_tax'] = 0;
            $data['tax_pb'] = 0;
            $data['tax_pph'] =  0;
            $data['balance_after_tax'] =  0;
            $data['tax_ppn'] =  0;
            $data['net_after_tax'] =  0;
            $data['net_after_tax_ppn'] = 0;


            $branch_id = Session::get('branch_id');
            $start_date = $request->start_date;
            $end_date = $request->end_date;



            $sql_rev = DB::select("select sum(total) as revenue from revenue where date(`date`) between date('".$start_date."') and date('".$end_date."') and branch_id= '".$branch_id."' and deleted_at is null");
            foreach ($sql_rev as $key) {
                $data['revenue'] = $key->revenue;
            }

            $sql_cogs = DB::select("SELECT
                'Suplay Sumoboo' AS `description`,
                sum(
                    (
                        ifnull(
                            item_central_detail.price * item_central_detail.quantity_actual,
                            0
                        )
                    )
                ) AS `amount`
            FROM
                item_central
            INNER JOIN item_central_detail ON item_central.id = item_central_detail.item_central_id
            INNER JOIN item ON item_central_detail.item_id = item.id
            WHERE
                (
                    date(item_central.date) BETWEEN date('".$start_date."')
                    AND date('".$end_date."')
                )
            AND item_central.branch_id = '".$branch_id."'
            AND item_central.deleted_at IS NULL
            AND item_central.status_code = 'Pay'
            AND item.item_category_id = 2
            UNION ALL
            SELECT 'Consumable direct to vendor' AS `type`,
                   sum( `invoice_direct`.`invoice_total` + IFNULL(invoice_direct.additional_cost, 0) ) AS `amount`
            FROM `invoice_direct` where MONTH(`invoice_date`) = MONTH('".$end_date."') and YEAR(invoice_date) = YEAR('".$end_date."')
            and deleted_at is null and paid=1 and branch_id = '".$branch_id."'
            UNION ALL
             SELECT
             'Consumable (supporting restorant)' AS `type`,
                sum(
                    (
                        ifnull(
                            item_central_detail.price * item_central_detail.quantity_actual,
                            0
                        )
                    )
                ) AS `amount`
            FROM
                item_central
            INNER JOIN item_central_detail ON item_central.id = item_central_detail.item_central_id
            INNER JOIN item ON item_central_detail.item_id = item.id
            WHERE
                (
                    date(item_central.date) BETWEEN date('".$start_date."')
                    AND date('".$end_date."')
                )
            AND item_central.branch_id = '".$branch_id."'
            AND item_central.deleted_at IS NULL
            AND item_central.status_code = 'Pay'
            AND item.item_category_id = 1

            UNION ALL
             SELECT
             'Transportation' AS `type`,
                sum(
                    (
                        ifnull(
                            item_central.total_transport,
                            0
                        )
                    )
                ) AS `amount`
            FROM
                item_central 
            WHERE
                (
                    date(item_central.date) BETWEEN date('".$start_date."')
                    AND date('".$end_date."')
                )
            AND item_central.branch_id = '".$branch_id."'
            AND item_central.deleted_at IS NULL
            AND item_central.status_code = 'Pay'");

            foreach ($sql_cogs as $key) {
                if ($key->description == 'Suplay Sumoboo') {
                    $data['suplay_sumobo'] = $key->amount;
                }
                if ($key->description == 'Consumable direct to vendor') {
                    $data['cons_direct_vendor'] = $key->amount;
                }
                 if ($key->description == 'Consumable (supporting restorant)') {
                    $data['cons_supp'] = $key->amount;
                }

                if ($key->description == 'Transportation') {
                    $data['transportation'] = $key->amount;
                }
            }



            $sql_2 = DB::select("select description,sum(amount) as amount from (SELECT franchise_fee.branch_id,
              'Franchise Fee Royalti Omset' AS description,
              franchise_fee.start_date,
              franchise_fee.royalty_value AS amount
               FROM franchise_fee where date(start_date)>='".$start_date."' and date(end_date) <='".$end_date."' and deleted_at is null and branch_id = '".$branch_id."'

               UNION ALL

               SELECT
                    cash_operational.branch_id,
                    'Cash Operational' AS description,
                    cash_operational.start_date,
                    cash_operational.budget_issued AS amount
                FROM
                    cash_operational
                WHERE
                    start_date >= '".$start_date."'
                AND end_date <= '".$end_date."'
                AND deleted_at IS NULL
                AND branch_id = '".$branch_id."'

               UNION ALL 

               SELECT
                    cashbond.branch_id,
                    'Cash Bahan Baku' AS description,
                    cashbond.start_date AS date,
                    cashbond.cashout AS amount
                FROM
                    cashbond
                where MONTH(`start_date`) = MONTH('".$end_date."') and YEAR(start_date) = YEAR('".$end_date."')  and deleted_at is null and branch_id = '".$branch_id."'
               UNION ALL 
               SELECT a.branch_id,
                'Salary' AS description,
                a.paid_date AS DATE,
                sum(b.slr_total) as total_salary
                FROM payroll a join payroll_detail b on (a.id = b.payroll_id) where 
                (a.`month`) = MONTHNAME('".$end_date."') AND (a.`year`) = YEAR('".$end_date."')
                 and a.deleted_at is null  and a.branch_id = '".$branch_id."' AND b.deleted_at IS NULL
               UNION ALL 

               SELECT invoice.branch_id,
                                invoice_type.inv_type_name AS description,
                                invoice.invoice_date AS DATE,
                                invoice.invoice_total + IFNULL(invoice.additional_cost, 0) AS amount
               FROM invoice
               INNER JOIN invoice_type ON invoice.invoice_type_id = invoice_type.id where
              (invoice.`month`) = MONTH('".$end_date."') AND (invoice.`year`) = YEAR('".$end_date."') and invoice.deleted_at is null and approved=1) a where a.branch_id = '".$branch_id."' group by description");

            foreach ($sql_2 as $key) {
                if ($key->description == 'Air') {
                    $data['air'] = $key->amount;
                }
                if ($key->description == 'Cash Bahan Baku') {
                    $data['cash_bahan_baku'] = $key->amount;
                }
                if ($key->description == 'Catering') {
                    $data['catring'] = $key->amount;
                }

                if ($key->description == 'Kost') {
                    $data['kost'] = $key->amount;
                }


                if ($key->description == 'Listrik') {
                    $data['listrik'] = $key->amount;
                }

                if ($key->description == 'Salary') {
                    $data['salary'] = $key->amount;
                }

                if ($key->description == 'Franchise Fee Royalti Omset') {
                    $data['franchise_fee'] = $key->amount;
                }

                 if ($key->description == 'Cash Operational') {
                    $data['cash_operational'] = $key->amount;
                }

                if ($key->description == 'IPL') {
                    $data['ipl'] = $key->amount;
                }

                if ($key->description == 'Satpam') {
                    $data['satpam'] = $key->amount;
                }

                if ($key->description == 'Internet') {
                    $data['internet'] = $key->amount;
                }
            }
                 $sql_3 = DB::select("select * from op where start_date='".$start_date."' and end_date='".$end_date."' and deleted_at is null AND op.branch_id = ".Session::get('branch_id')."");

                 // dd($sql_3);

                foreach ($sql_3 as $key) {
                    $data['franchise_amortization'] = $key->franchise_amortization;
                    $data['asset_deprecation'] = $key->asset_deprecation;
                    $data['rent_ruko'] = $key->rent_ruko;

                }
                $data['total_o_p'] = $data['franchise_amortization'] +
                $data['asset_deprecation'] + $data['rent_ruko'] + $data['franchise_fee'] +
                $data['cash_op'] +
                $data['cash_bahan_baku'] + $data['salary'] + $data['listrik'] + $data['catring'] +
                $data['air']  + $data['kost'] + $data['solar'] + $data['ipl'] +  $data['satpam'] +$data['misc']+ $data['internet'] ;

                $data['tax_royalti'] = ($data['franchise_fee'] * 0.15) / 2;


                $data['cogs'] = $data['suplay_sumobo']+$data['cons_direct_vendor']+$data['cons_supp'];
                $data['gross_profit'] =  $data['revenue'] - $data['cogs'];

                $data['net_before_tax'] = $data['gross_profit'] - $data['total_o_p'];

                $data['tax_pb'] = ($data['revenue'] /100) * 10;

                if($data['revenue'] >= 0 && $data['revenue'] <= 50000000){
                   $pre_pph = 5; 
                }
                elseif($data['revenue'] >= 50000000 && $data['revenue'] <= 250000000){
                   $pre_pph = 15; 
                }
                elseif($data['revenue'] >= 250000000 && $data['revenue'] <= 500000000){
                    $pre_pph = 25;
                }
                else{
                    $pre_pph = 30;
                };

                $data['tax_pph'] = ($data['net_before_tax'] /100) * $pre_pph;

                $data['net_after_tax'] =  ($data['net_before_tax']) - ($data['net_before_tax'] /100) * $pre_pph;

                $data['tax_ppn'] = $data['net_before_tax'] * 0.1;

                $data['net_after_tax_ppn'] = $data['net_before_tax'] - $data['tax_ppn'];
                


        }else{
                         $start_date = null;
                        $end_date = null;
        }





        return view('editor.profit_lost.index',compact('bulan','tahun','data','start_date','end_date'));
    }
}
