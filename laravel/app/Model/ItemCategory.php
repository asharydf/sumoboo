<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemCategory extends Model
{
	use SoftDeletes;
	protected $table = 'item_category';
	protected $dates = ['deleted_at'];

	
	public function item_type()
	{
		return $this->belongsTo('App\Model\ItemType', 'item_type_id', 'id');
	}

	public function invoice_type()
	{
		return $this->belongsTo('App\Model\InvoiceType', 'item_category_id', 'id');
	}

}
