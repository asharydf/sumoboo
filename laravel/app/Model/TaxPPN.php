<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxPPN extends Model
{
    use SoftDeletes;

    protected $table = 'tax_ppn';
    protected $dates = ['deleted_at'];

    public function revenue()
    {
    	return $this->belongsTo('App\Model\Revenue', 'revenue_id', 'id');
    }

    public function branch()
    {
    	return $this->belongsTo('App\Model\Branch', 'branch_id', 'id');
    }
}
