<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Directory Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the directory path for the assets you use such as
    | CSS, js, plug-ins, etc.
    |
    */

    'path' => [
        'uploads' => '/sumoboo 2/sumoboo/uploads',
        'bootstrap' => '/sumoboo 2/sumoboo/laravel/bootstrap',
        'css' => '/sumoboo 2/sumoboo/assets/css',
        'scss' => '/sumoboo 2/sumoboo/assets/lte_sass/build/scss',
        'img' => '/sumoboo 2/sumoboo/assets/img',
        'js' => '/sumoboo 2/sumoboo/assets/js',
        'plugin' => '/sumoboo 2/sumoboo/assets/plugins',
    ],

];