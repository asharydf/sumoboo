<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Directory Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the directory path for the assets you use such as
    | CSS, js, plug-ins, etc.
    | 
    */

    'path' => [
        'uploads' => '/sumoboo/public/uploads',
        'bootstrap' => '/sumoboo/public/laravel/bootstrap',
        'css' => '/sumoboo/public/assets/css',
        'scss' => '/sumoboo/public/assets/lte_sass/build/scss',
        'img' => '/sumoboo/public/assets/img',
        'js' => '/sumoboo/public/assets/js',
        'plugin' => '/sumoboo/public/assets/plugins',
    ],

];