@extends('layouts.editor.template')
@section('content')



    <!-- Content Header (Page header) -->
    <section class="content-header hidden-xs">

        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-lg-12">
            <div class="row">
              <form method="post">
                {{ csrf_field() }}
              <div class="form-group">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <select class="form-control"  name="selectMonth" id="selectMonth">
                      
                    </select>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-4">
                  <button type="button" class="btn btn-primary" id="btnFilter"><i class="fa fa-filter"></i> Filter</button>
                </div>
              </div>
            </form>
            </div>
          </div>
        <div class="col-lg-12">
          <canvas id="barChart"> </canvas>
        </div>

        <div class="col-lg-12">
          <canvas id="barChartMulti"></div>
        </div>
      </div>
    </section>
    <!-- /.content -->

@stop
