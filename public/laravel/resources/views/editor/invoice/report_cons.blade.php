@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-dot-circle-o"></i> Item Central</a></li>
	</ol>
</section>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>



<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>

								<i class="fa fa-file-text-o"></i> Invoice Report Consumable

						</h2>

							<br>
							<form class="form-inline" method="post">
								{{ csrf_field() }}

								<div class="form-group">
									<select name="item" class="form-control selectpicker" data-live-search="true" >

										@foreach($vendor_name as $key)
											@if(isset($vendor_item[$key->vendor_name]))
												<optgroup label="{{ $key->vendor_name }}">
													@foreach($vendor_item[$key->vendor_name] as $key)

														@if($key->id == $item_id)
															<option style="color:black;" value="{{ $key->id }}" selected>{{ $key->item_name }}</option>
														@else
															<option style="color:black;" value="{{ $key->id }}" >{{ $key->item_name }}</option>
														@endif
													@endforeach
												</optgroup>
											@endif

										@endforeach




									</select>

								</div>


								 <div class="form-group">
                                  {{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'placeholder' => 'Start date', 'required' => 'true', 'id' => 'start_date')) }}<br/>
                                </div>
                                <div class="form-group">
                                  {{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'placeholder' => 'End date', 'required' => 'true', 'id' => 'end_date')) }}<br/>
                                </div>



								<button type="submit" class="btn btn-default">Filter</button>

							</form>
							<br>
						<div class="x_content">
							<table id="item_centralTablex" class="table dataTable rwd-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Month</th>

										<th>Total</th>

									</tr>
									</thead>
									<tbody>
									@if(isset($reports))
										@foreach ($reports as $report)
											<tr>

												<td>{{ $loop->iteration }}</td>
												<td>{{ $report->bulan }}</td>

												<td>{{ number_format($report->jml,0) }}</td>


                                            </tr>
										@endforeach

									@endif

									</tbody>
									</table>
								</div>

							</div>
						</div>



					</div>
				</div>
				<div id="curve_chart" style="width: 900px; height: 500px"></div>
			</section>
		</section>


@stop

@section('scripts')

<script type="text/javascript">
        @if ($graph == '')
            alert('Data not found');
        @endif
         @if(isset($graph) && $graph != '')
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Total'],

          {!! $graph !!}

        ]);

        var options = {
          title: 'Item Graph',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
</script>
  @endif
@stop