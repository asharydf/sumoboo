@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.invoice.index') }}"><i class="fa fa-file-text-o"></i> Invoice Utilization</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
							@if(isset($invoice))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Invoice Utilization
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error') 
							{!! Form::model($invoice, array('route' => ['editor.invoice.updatepaid', $invoice->id], 'method' => 'PUT', 'class'=>'update', 'files'=>'true', 'id'=>'form_invoice'))!!}

							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('Invoice Type') }}
								{{ Form::select('invoice_type_id', $invoice_type_list, old('invoice_type_id'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_date', 'Invoice Date') }}  
								<input type="text" class="form-control" name="invoice_date" value="{{date('d-M-Y', strtotime($invoice->invoice_date))}}" disabled="disabled"><br/> 

								{{ Form::label('month', 'Month') }}
								{{ Form::select('month', $month_list, old('month'), ['class' => 'form-control', 'placeholder' => 'Select Month', 'id' => 'month', 'disabled' => 'disabled']) }}<br>

								{{ Form::label('year', 'Year') }}
								{{ Form::text('year', old('year'), array('class' => 'form-control', 'placeholder' => 'Year*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('Vendor') }}
								{{ Form::select('vendor_id', $vendor_list, old('vendor_id'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_bank', 'Bank') }}
								{{ Form::text('invoice_bank', old('invoice_bank'), array('class' => 'form-control', 'placeholder' => 'Bank*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_rekening', 'Rek No') }}
								{{ Form::text('invoice_rekening', old('invoice_rekening'), array('class' => 'form-control', 'placeholder' => 'Rek No*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_total', 'Total') }}  
								<input type="text" class="form-control" name="invoice_total" value="{{number_format($invoice->invoice_total,0)}}" disabled="disabled"><br/>

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/> 

								{{ Form::label('paid_date', 'Paid Date') }}
								{{ Form::text('paid_date', old('paid_date'), array('class' => 'form-control', 'placeholder' => 'Paid Date*', 'required' => 'true')) }}<br/> 

								{{ Form::label('reference_no', 'Reference No') }}
								{{ Form::text('reference_no', old('reference_no'), array('class' => 'form-control', 'placeholder' => 'Reference No*', 'required' => 'true', 'id' => 'reference_no')) }}<br/>

								{{ Form::label('attachment_receipt', 'Attachment Receipt') }}
								{{ Form::file('attachment_receipt') }}<br/>

								<button type="button" data-toggle="modal" data-target="#modal_invoice" class="btn btn-success pull-right"><i class="fa fa-check"></i> Paid</button>
								<a href="{{ URL::route('editor.invoice.bank') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_invoice">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Paid this invoice?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>  
	$('#btn_submit').on('click', function()
	{
		$('#form_invoice').submit();
	});
</script> 
@stop