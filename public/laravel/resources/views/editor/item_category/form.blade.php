<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.item_category.index') }}"><i class="fa fa-cubes"></i> Type Item</a></li>
  </ol>
</section>
<section class="content">
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($item_category))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						&nbsp;Item Category
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($item_category))
						{!! Form::model($item_category, array('route' => ['editor.item_category.update', $item_category->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_itemcategory'))!!}
						@else
						{!! Form::open(array('route' => 'editor.item_category.store', 'class'=>'create', 'id' => 'form_itemcategory'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
							{{ Form::label('category_name', 'Name') }}
							{{ Form::text('category_name', old('category_name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

							{{ Form::label('category_desc', 'Description') }}
							{{ Form::text('category_desc', old('category_desc'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}<br/>

							{{ Form::label('Type Item') }}
		                    	{{ Form::select('item_type_id', $item_type_list, old('item_type_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>
							<button type="button" data-toggle="modal" data-target="#modal_itemcategoty" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.item_category.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_itemcategoty">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this item category?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_itemcategory').submit();
	});
</script>
@stop