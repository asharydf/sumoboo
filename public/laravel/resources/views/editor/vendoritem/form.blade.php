<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.item_type.index') }}"><i class="fa fa-cubes"></i> Type Item</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-6">
					<div class="x_panel">
						<h2>
							@if(isset($vendoritem))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Vendor Item
						</h2>
						<hr>
						<div class="x_content"> 
							<div class="col-md-12 col-xs-12">
								<h3><i class="fa fa-info-circle"></i> Vendor</h3>
								<table class="table">
									<tr>
										<th width="40%">Name</th>
										<td width="60%">{{$vendor->vendor_name}}</td>
									</tr>
									<tr>
										<th>Phone</th>
										<td>{{$vendor->vendor_phone}}</td>
									</tr>
									<tr>
										<th>Address</th>
										<td>{{$vendor->vendor_address}}</td>
									</tr>
									<tr>
										<th>Email</th>
										<td>{{$vendor->vendor_email}}</td>
									</tr>
									<tr>
										<th>Bank</th>
										<td>{{$vendor->vendor_bank}}</td>
									</tr>
									<tr>
										<th>Rekening</th>
										<td>{{$vendor->vendor_rekening}}</td>
									</tr>
									<tr>
										<th>PIC</th>
										<td>{{$vendor->vendor_pic}}</td>
									</tr>
									<tr>
										<th>PIC Num</th>
										<td>{{$vendor->vendor_pic_num}}</td>
									</tr>  
								</table>
							</div> 


							<div class="col-md-12 col-xs-12">
								<h3><i class="fa fa-info-circle"></i> Item</h3>

								{!! Form::model($vendor, array('route' => ['editor.vendoritem.store', $vendor->id], 'method' => 'PUT', 'class'=>'cerate', 'id'=>'form_vendoritem'))!!}
								{{ csrf_field() }}

								{{ Form::label('item_id', 'Item') }}
								{{ Form::select('item_id', $item_list, old('item_id'), array('class' => 'form-control', 'required' => 'true', 'id' => 'item_id', 'placeholder' => 'Select Item')) }}<br/> 

								<button type="button" data-toggle="modal" class="btn btn-success pull-right" onclick="validate();" id="btn_save"><i class="fa fa-plus"></i> Add</button>
								<a href="{{ URL::route('editor.vendoritem.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
								{!! Form::close() !!}
								<br/><br/>
								<table class="table">
									<tr>
										<th width="40%">#</th>
										<th width="50%">Item Name</th>
										<th width="10%">Action</th>
									</tr>

									@foreach($vendor_item as $key => $vendor_items)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$vendor_items->item_name}}</td>
										<td> 	
											@actionStart('vendoritem', 'delete')  
											{!! Form::open(array('route' => ['editor.vendoritem.delete', $vendor_items->id], 'method' => 'delete', 'class'=>'delete'))!!}
											{{ csrf_field() }}	 
											<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></button> 
											{!! Form::close() !!} 
											@actionEnd 
 
										</td>
									</tr> 
									@endforeach
								</table>
							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_vendoritem">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Add this item to {{$vendor->vendor_name}}?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_vendoritem').submit();
	});

	function validate(){  
		$('#modal_vendoritem').modal('toggle');
		$('#modal_vendoritem').modal('show'); 
	}
	
	$(".delete").on("submit", function(){
		return confirm("Delete this item?");
	});
</script>

@stop
