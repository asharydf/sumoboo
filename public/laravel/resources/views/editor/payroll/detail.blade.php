@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.payroll.index') }}"><i class="fa fa-list-ul"></i> Payroll List</a></li>
    <li class="active"><i class="fa fa-search"></i> Detail</li>
  </ol>
</section>
@actionStart('payroll', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-search"></i> Payroll Detail
		                	@if($payroll->status == 0)
			                	@actionStart('payroll', 'update')
			                	<a href="{{ URL::route('editor.payroll.edit', [$payroll->id]) }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-pencil"></i> Edit</a> 
			                	@actionEnd
		                	@endif
	                	</h2>
		                <hr>
			           	<div class="x_content">
			           		{{-- PAYROLL INFO --}}
			           		<div class="col-md-6 col-xs-12">
				                <h3><i class="fa fa-info-circle"></i> Info</h3>
				                <table class="table">
				                	<tr>
				                		<th width="40%">Year</th>
				                		<td width="60%">{{$payroll->year}}</td>
				                	</tr>
				                	<tr>
				                		<th>Month</th>
				                		<td>{{$payroll->month}}</td>
				                	</tr>
				                	<tr>
				                		<th>Default Work Days</th>
				                		<td>{{$payroll->default_work_days}}</td>
				                	</tr>
				                	<tr>
				                		<th>Comment</th>
				                		<td>{{$payroll->comment}}</td>
				                	</tr>
				                	<tr>
				                		<th>Approval Date</th>
				                		<td>
				                			@if(!$payroll->approved_date)
											<span class="label label-danger"> UNAPPROVED </span>
											@elseif($payroll->approved_date)
											<span class="label label-success"> {{date('D, d-m-Y', strtotime($payroll->approved_date))}} </span>
											@endif
				                		</td>
				                	</tr>
				                	<tr>
				                		<th>Payment Date</th>
				                		<td>
				                			@if(!$payroll->paid_date)
											<span class="label label-danger"> UNPAID </span>
											@elseif($payroll->paid_date)
											<span class="label label-success"> {{date('D, d-m-Y', strtotime($payroll->paid_date))}} </span>
											@endif
				                		</td>
				                	</tr>
				                </table>
			                </div>
			                {{-- PAYROLL INFO --}}

			                {{-- PAYROLL DETAIL TABLE --}}
			                <div class="col-md-12  col-xs-12">
			                	<div class="div_overflow">
									<div style="overflow-x:auto;">
				                	<table class="table table-hover" id="table_payroll_detail" >
				                	<thead>
				                		<tr>
				                			<tr>
												<th rowspan="2">#</th>
												<th rowspan="2">Name</th>
												<th rowspan="2">Position</th>
												<th rowspan="2">Work Days</th>
												<th rowspan="2">Basic Salary</th>
												<th rowspan="2">Voucher</th>
												<th colspan="4"><center>Additional</center></th>
												<th rowspan="2">Sallary</th> 
												<th colspan="5"><center>Reduction</center></th> 
												<th rowspan="2">THP</th>
											</tr> 
											<tr>
												<th>Holiday Allowance</th>
												<th>Transport</th>
												<th>Meal</th>  
												<th>Peng Deposit</th> 
												<th>Cash Bond</th>
												<th>Deposit</th> 
												<th>Dormitory</th>
												<th>Additional Cost</th>  
												<th>Notes</th>  
											</tr>
				                		</tr>
				                	</thead>
				                	<tbody>
				                		@foreach($payroll->payroll_detail as $key => $payroll_detail)
				                		<tr>
				                			<td>{{$key+1}}</td>
				                			<td>{{$payroll_detail->employee->emp_full_name}}</td>
				                			<td>{{$payroll_detail->employee->emp_position}}</td>
				                			<td>{{$payroll_detail->workdays}}</td>
				                			<td>{{number_format($payroll_detail->slr_basic)}}</td>  
											<td>{{number_format($payroll_detail->slr_voucher)}}</td>
				                			<td>{{number_format($payroll_detail->slr_thr)}}</td>
				                			<td>{{number_format($payroll_detail->slr_transport)}}</td>
				                			<td>{{number_format($payroll_detail->slr_tunjangan_makan)}}</td>
				                			<td>{{number_format($payroll_detail->slr_peng_deposit)}}</td>
				                			<td>{{number_format($payroll_detail->slr_total,0)}}</td> 
				                			<td>{{number_format($payroll_detail->slr_cashbond)}}</td>
				                			<td>{{number_format($payroll_detail->slr_pot_deposit)}}</td>
				                			<td>{{number_format($payroll_detail->boarding_house)}}</td>
				                			<td>{{number_format($payroll_detail->additional_cost)}}</td>
				                			<td>{{$payroll_detail->add_cost_desc}}</td> 
				                			<td>{{number_format($payroll_detail->slr_thp)}}</td> 
				                		</tr>
				                		@endforeach
				                	</tbody>
				                	<tfoot>
					                	<tr>
					                		<th>Total</th>
					                		<td></td>
					                		<td></td>
					                		<td></td>
					                		<td><b>{{number_format($payroll->payroll_detail->sum('slr_basic'))}}</b></td>
											<td><b>{{number_format($payroll->payroll_detail->sum('slr_voucher'))}}</b></td>
					                		<td><b>{{number_format($payroll->payroll_detail->sum('slr_thr'))}}</b></td> 
					                		<td><b>{{number_format($payroll->payroll_detail->sum('slr_transport'))}}</b></td>
					                		<td><b>{{number_format($payroll->payroll_detail->sum('slr_tunjangan_makan'))}}</b></td>
					                		<td><b>{{number_format($payroll->payroll_detail->sum('slr_peng_deposit'))}}</b></td>
					                		<td><b> 
					                			{{number_format($payroll->payroll_detail->sum('slr_total'))}} 
					                		</b></td>
					                		<td><b>{{number_format($payroll->payroll_detail->sum('slr_cashbond'))}}</b></td>
					                		<td><b>{{number_format($payroll->payroll_detail->sum('slr_pot_deposit'))}}</b></td>
					                		<td><b>{{number_format($payroll->payroll_detail->sum('boarding_house'))}}</b></td>
					                		<td><b>{{number_format($payroll->payroll_detail->sum('additional_cost'))}}</b></td>
					                		<td></td> 
					                		<td><b>
					                		{{number_format($payroll->payroll_detail->sum('slr_thp'))}}</b></td>
					                	</tr> 	
				                	</tfoot>
				                	</table>
									</div>
				                </div>
			                </div> 
			                {{-- PAYROLL DETAIL TABLE --}}

			                {{-- APPROVAL AND PAYMENT --}}
			                <div class="col-md-12 col-xs-12">
			                	@if($payroll->status == 0)
			                		@actionStart('payroll', 'submit')
				                	{!! Form::open(['route' => ['editor.payroll.submit', $payroll->id], 'method' => 'PUT', 'id' => 'form_submit']) !!}
				                	<button type="button" class="btn btn-lg btn-success" id="btn_submit">
				                		<i class="fa fa-check"></i> Submit
			                		</button>
				                	{!! Form::close() !!}
				                	@actionEnd

			                	@elseif($payroll->status == 1)
			                		@actionStart('payroll', 'paid')
				                	{!! Form::open(['route' => ['editor.payroll.finance_approve', $payroll->id], 'method' => 'PUT', 'id' => 'form_finance']) !!}
				                	<input type="hidden" name="review" value="0" id="finance_review">
			                		<table>
		                				<tr>
		                					<td>
							                	<button type="button" class="btn btn-lg btn-danger" id="btn_finance_reject">
							                		<i class="fa fa-remove"></i> Reject
						                		</button>
						                	</td>
						                	<td>
							                	<button type="button" class="btn btn-lg btn-success" id="btn_finance_approve">
							                		<i class="fa fa-check"></i> Approve
						                		</button>
						                	</td>
					                	</tr>	
				                	</table>
				                	{!! Form::close() !!}
				                	@actionEnd

			                	@elseif($payroll->status == 2)
			                		@actionStart('payroll', 'issued')
				                	{!! Form::open(['route' => ['editor.payroll.owner_approve', $payroll->id], 'method' => 'PUT', 'id' => 'form_owner']) !!}
				                	<button type="button" class="btn btn-lg btn-success" id="btn_owner">
				                		<i class="fa fa-check"></i> Issued
			                		</button>
				                	{!! Form::close() !!}
				                	@actionEnd

			                	@elseif($payroll->status == 3)
			                		@actionStart('payroll', 'paid')
				                	{!! Form::open(['route' => ['editor.payroll.finance_payment', $payroll->id], 'method' => 'PUT', 'id' => 'form_paid', 'files' => 'true']) !!}
				                	
				                	{{ Form::label('attachment_receipt', 'Attachment Receipt') }}
									{{ Form::file('attachment_receipt') }}<br/>

				                	<button type="button" class="btn btn-lg btn-success" id="btn_paid">
				                		<i class="fa fa-check"></i> Paid
			                		</button>
				                	{!! Form::close() !!}
				                	@actionEnd

			                	@endif 
			                	<br> 
								@if(isset($payroll)) 
								<span class="label label-default"><i class="fa fa-time"></i>
									Updated at: {{date("d M Y", strtotime($payroll_usr->updated_at))}}
								</span><br/>
								<span class="label label-default"><i class="fa fa-time"></i>
									Updated by: {{$payroll_usr->username}}
								</span>
								@endif
			                </div>
			                {{-- APPROVAL AND PAYMENT --}}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_summary">
  	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
    		    <h5 class="modal-title"><i class="fa fa-warning"></i> Confirmation!</h5>
      		</div>
      		<div class="modal-body">
        		<div id="modal_summary_body"></div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" id="btn_confirm">YES</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
$('#btn_submit').on('click', function()
{
	$('#modal_summary_body').html('Submit Payroll?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_submit').submit();
	});
});

$('#btn_finance_reject').on('click', function()
{
	$('#finance_review').val('0');
	$('#modal_summary_body').html('Reject Payroll?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance').submit();
	});
});

$('#btn_finance_approve').on('click', function()
{
	$('#finance_review').val('1');
	$('#modal_summary_body').html('Approve Payroll?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance').submit();
	});
});

$('#btn_owner').on('click', function()
{
	$('#modal_summary_body').html('Issue Payroll?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_owner').submit();
	});
});

$('#btn_paid').on('click', function()
{
	$('#modal_summary_body').html('Pay Payroll?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_paid').submit();
	});
});
</script>
@stop