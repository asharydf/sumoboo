
@if(isset($data))
<table id="table_franchise_feex" class="table dataTable rwd-table table-bordered" >
    <thead>
        <tr >
            <th width="5%">#</th>
            <th>Description</th>
            <th>Percentage % </th>
            <th>IDR</th>
        </tr>
    </thead>
    <tbody>
        <tr style="border-color: black;">
            <td><b>1.</b></td>
            <td><b>Revenue</b></td>
            <td>100%</td>
            <td>{{ number_format($data['revenue']) }}</td>
        </tr>
        {{--  cogs --}}
        <tr>
            <td><b>2.</b></td>
            <td colspan="3"><b>COST OF GOOD SOLD</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Suplay Sumobo</td>
            <td>{{ number_format(($data['suplay_sumobo']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['suplay_sumobo']) }}</td>
        </tr>
          <tr>
            <td></td>
            <td>Consumable (supporting restorant)</td>
            <td>{{ number_format(($data['cons_supp']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['cons_supp']) }}</td>
        </tr>

        <tr>
            <td></td>
            <td>Transportation Sumobo</td>
            <td>0%</td>
            <td>0</td>
        </tr>

        <tr>
            <td></td>
            <td>Consumable direct to vendor</td>
           <td>{{ number_format(($data['cons_direct_vendor']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['cons_direct_vendor']) }}</td>
        </tr>


        <tr style="background-color:#ffff56;">
            <td></td>
            <td ><b>Total COST OF GOOD SOLD</b></td>
            <td>{{ number_format( ($data['cogs'] /$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['cogs'])  }}</td>
        </tr>
        <tr style="background-color:#ffff56;font-weight: bold">
            <td><b>3</b></td>
            <td><b>Gross Profit</b></td>

            <td>{{ number_format(($data['gross_profit'] /$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['gross_profit'])  }}</td>
        </tr>
        {{--  operating --}}
        <tr>
            <td><b>4.</b></td>
            <td colspan="3"><b>Operating Expenses</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Franchise Amortization</td>
            <td>{{ number_format(($data['franchise_amortization']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['franchise_amortization']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Asset Depreciation</td>
            <td>{{ number_format(($data['asset_deprecation']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['asset_deprecation']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Rent Ruko</td>
            <td>{{ number_format(($data['rent_ruko']/$data['revenue']) * 100) }}%</td>
             <td>{{ number_format($data['rent_ruko']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Franchise Fee Royalti Omset</td>
            <td>{{ number_format(($data['franchise_fee']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['franchise_fee']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Cash Operational</td>
            <td>{{ number_format(($data['cash_op']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['cash_op']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Cash Bahan baku</td>
            <td>{{ number_format(($data['cash_bahan_baku']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['cash_bahan_baku']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Salary</td>
            <td>{{ number_format(($data['salary']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['salary']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="3"><b>Utility</b></td>
        </tr>
        <tr>
            <td></td>
            <td>- Electricity</td>
              <td>{{ number_format(($data['listrik']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['listrik']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>- Catering</td>
            <td>{{ number_format(($data['catring']/$data['revenue']) * 100) }}%</td>
               <td>{{ number_format($data['catring']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>- PDAM</td>
            <td>{{ number_format(($data['air']/$data['revenue']) * 100) }}%</td>
              <td>{{ number_format($data['air']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>- Kost</td>
            <td>{{ number_format(($data['kost']/$data['revenue']) * 100) }}%</td>
               <td>{{ number_format($data['kost']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>- Solar Genset</td>
            <td>{{ number_format(($data['solar']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['solar']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>- IPL</td>
             <td>{{ number_format(($data['ipl']/$data['revenue']) * 100) }}%</td>
               <td>{{ number_format($data['ipl']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>- Expense Satpam</td>
             <td>{{ number_format(($data['satpam']/$data['revenue']) * 100) }}%</td>
               <td>{{ number_format($data['satpam']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>- Misscellaneous</td>
            <td>{{ number_format(($data['misc']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['misc']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>- Indihome</td>
            <td>{{ number_format(($data['internet']/$data['revenue']) * 100) }}%</td>
               <td>{{ number_format($data['internet']) }}</td>
        </tr>
        <tr style="background-color:#ffff56;font-weight: bold">
            <td></td>
            <td><b>TOTAL OPERATING EXPENSES</b></td>
            <td>{{ number_format(($data['total_o_p']/$data['revenue']) * 100) }}%</td>
               <td>{{ number_format($data['total_o_p']) }}</td>
        </tr>
        <tr style="background-color:#ffff56;font-weight: bold">
            <td><b>5</b></td>
            <td><b>NET PROFIT BEFORE TAX </b></td>

            <td>{{ number_format( ($data['net_before_tax']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['net_before_tax']) }}</td>
        </tr>
         <tr>
            <td></td>
            <td>TAX PB1 (10%)</td>
             <td>{{ number_format(($data['tax_pb']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['tax_pb']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>tax 7.5% royalti fee</td>
            <td>{{ number_format(($data['tax_royalti']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['tax_royalti']) }}</td>
        </tr>

        <tr>
            <td><b>6</b></td>
            <td><b>NET PROFIT AFTER TAX BALANCE </b></td>
             <td>{{ number_format(($data['net_after_tax']/$data['revenue']) * 100) }}%</td>
            <td>{{ number_format($data['net_after_tax']) }}</td>
        </tr>
    </tbody>
</table>
@endif