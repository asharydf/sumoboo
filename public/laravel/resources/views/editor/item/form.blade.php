<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.item.index', [$id_item_type]) }}"><i class="fa fa-cube"></i> Item</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
						
							@if(isset($item))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Item
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error')
							@if(isset($item))
							{!! Form::model($item, array('route' => ['editor.item.update', $item->item_type_id, $item->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_item'))!!}
							@else
							{!! Form::open(array('route' => ['editor.item.store', $id_item_type], 'class'=>'create', 'id'=>'form_item'))!!}
							@endif
							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('item_name', 'Name') }}
								{{ Form::text('item_name', old('item_name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

								{{ Form::label('uom', 'UOM') }}
								{{ Form::text('uom', old('uom'), array('class' => 'form-control', 'placeholder' => 'UOM*', 'required' => 'true')) }}<br/>

								{{ Form::label('price', 'Price') }}
								{{ Form::number('price', old('price'), array('class' => 'form-control', 'placeholder' => 'Price*', 'required' => 'true')) }}<br/>

								{{ Form::label('item_desc', 'Description') }}
								{{ Form::text('item_desc', old('item_desc'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}<br/>

								{{ Form::label('Category Item') }}
			                    	{{ Form::select('item_category_id', $item_cat_list, old('item_category_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

								<button type="button" data-toggle="modal" data-target="#modal_item" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
								<a href="{{ URL::route('editor.item.index', [$id_item_type]) }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>

							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_item">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this item?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">

	$('#btn_submit').on('click', function()
	{
		$('#form_item').submit();
	});
</script>
@stop