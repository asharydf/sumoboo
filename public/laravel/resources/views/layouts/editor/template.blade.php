<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="{{Config::get('constants.path.img')}}/favicon.png" type="image/gif" sizes="16x16">
  <title>Sumoboo | Editor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{Config::get('constants.path.bootstrap')}}/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{Config::get('constants.path.scss')}}/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.css')}}/skins/_all-skins.min.css"> -->
  <!-- iCheck -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/morris/morris.css"> -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- dataTables -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.css" />
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.css">
  <!-- Datetime Picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datetimepicker/bootstrap-datetimepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datatables/fixedColumns.dataTables.min.css">

<link rel="stylesheet" href="{{Config::get('constants.path.css')}}/bootstrap-select.min.css" />
  <!-- jQuery 2.2.3 -->
  <script src="{{Config::get('constants.path.plugin')}}/jQuery/jquery-2.2.3.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="{{Config::get('constants.path.js')}}/bootstrap-select.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript">
    // insert commas as thousands separators
    function addCommas(n){
      var rx=  /(\d+)(\d{3})/;
      return String(n).replace(/^\d+/, function(w){
        while(rx.test(w)){
          w= w.replace(rx, '$1,$2');
        }
        return w;
      });
    }
// return integers and decimal numbers from input
// optionally truncates decimals- does not 'round' input
function validDigits(n, dec){
  n= n.replace(/[^\d\.]+/g, '');
  var ax1= n.indexOf('.'), ax2= -1;
  if(ax1!= -1){
    ++ax1;
    ax2= n.indexOf('.', ax1);
    if(ax2> ax1) n= n.substring(0, ax2);
    if(typeof dec=== 'number') n= n.substring(0, ax1+dec);
  }
  return n;
}
</script>
</head>
<body class="hold-transition skin-sumoboo sidebar-mini">
  <div class="wrapper">

    @include('layouts.editor.header')
    @include('layouts.editor.sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      @yield('content')
    </div>
    <!-- /.content-wrapper -->

    @include('layouts.editor.footer')

    {{-- @include('layouts.editor.controlbar') --}}

  </div>
  <!-- ./wrapper -->

  @yield('modal')


  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  @yield('scripts')
  <!-- Bootstrap 3.3.6 -->
  <script src="{{Config::get('constants.path.bootstrap')}}/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
  <!-- <script src="{{Config::get('constants.path.plugin')}}/morris/morris.min.js"></script> -->
  <!-- Sparkline -->
  <script src="{{Config::get('constants.path.plugin')}}/sparkline/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

  <!-- jQuery Knob Chart -->
  <script src="{{Config::get('constants.path.plugin')}}/knob/jquery.knob.js"></script>
  <!-- daterangepicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="{{Config::get('constants.path.plugin')}}/datepicker/bootstrap-datepicker.js"></script>
  <!-- datetimepicker -->
  <script src="{{Config::get('constants.path.plugin')}}/datetimepicker/bootstrap-datetimepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="{{Config::get('constants.path.plugin')}}/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="{{Config::get('constants.path.plugin')}}/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="{{Config::get('constants.path.js')}}/app.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- <script src="{{Config::get('constants.path.js')}}/pages/dashboard.js"></script> -->
  <!-- AdminLTE for demo purposes -->
  <script src="{{Config::get('constants.path.js')}}/demo.js"></script>
  <!-- ChartJS -->
  <script src="{{Config::get('constants.path.js')}}/chart/Chart.min.js"></script>
  <script src="{{Config::get('constants.path.js')}}/dataTables.fixedColumns.min.js"></script>
  
  <script type="text/javascript">

   $(function(){
     var url = "{{ url('api/revenue') }}";
     $('#btnFilter').on('click', function(event){
      //location.reload();

// var canvas = $('#barChartMulti')[0]; // or document.getElementById('canvas');
// canvas.width = canvas.width;
// var grapharea = document.getElementById("barChart").getContext("2d");
// var targetCanvas = document.getElementById('barChart').getContext('2d');
 // var myChart = new Chart(grapharea, { type: 'bar' });

// myChart.destroy();



      var monthInSelected = $('#selectMonth').val();
      var apiRevenue = url + '/' + monthInSelected;

       $.getJSON(apiRevenue, function(json) {
           var labels = [], data = [];
           console.log(apiRevenue);
           for(var i = 0; i < json.length; i++){
             labels.push(json[i].date);
             data.push(json[i].total);
           }
           var totalData = {
                 labels: labels,
                 datasets:[{
                    borderColor: "#FCEC0B",
                    fill: false,
                    pointBorderColor: "#FCEC0B",
                    pointBackgroundColor: "#FFF",
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBackgroundColor: "#FCEC0B",
                    pointHoverBorderColor: "#FFF",
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    label: 'Revenue',
                    data: data
                 }]
               };

               var chartBarId = document.getElementById('barChart').getContext('2d');
               if(window.bar != undefined)
                window.bar.destroy();

               window.bar = new Chart(chartBarId, {
                type: 'line',
                data: totalData,
                 options: {
                   animateScale : true,
                   tooltips:{
                     callbacks:{
                       title: function(tooltipItem, data){
                         var titleTooltips = moment(data['labels'][tooltipItem[0]['index']]).format('DD MMMM');
                         return titleTooltips;
                       },
                       label: function(tooltipItem, data){
                          var labelTooltips = data['datasets'][0]['data'][tooltipItem['index']].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                          return labelTooltips;
                        }
                       }
                   },
                   scales:{
                     xAxes:[{
                       type: 'time',
                       time:{
                         unit: 'day',
                         displayFormats:{
                          'millisecond': 'SSS [ms]',
                          'second': 'h:mm:ss a', // 11:20:01 AM
                          'minute': 'h:mm:ss a', // 11:20:01 AM
                          'hour': 'MMM D, hA', // Sept 4, 5PM
                          'day': 'DD MMM', // Sep 4 2015
                          'week': 'll', // Week 46, or maybe "[W]WW - YYYY" ?
                          'month': 'MMM YYYY', // Sept 2015
                          'quarter': '[Q]Q - YYYY', // Q3
                          'year': 'YYYY', // 2015
                         }
                       }
                     }],
                     yAxes:[{
                       ticks: {
                         beginAtZero:true,
                         min: 0,
                         max: 50000000,
                         callback: function(value, index, data){
                            value = value.toString();
                            value = value.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            return value;
                         }

                        }
                     }]
                   }
                 }
               });

       });

     });

    });

  </script>

<script  type="text/javascript">
var listMonth = Array.apply(0, Array(12)).map(function(key, value){
  return moment().month(value).format('MMMM')
});


$(function(){

  var month = "{{ url('api/month') }}";
  var exp = "{{ url('api/expense_output') }}";
  var rev = "{{ url('api/revenue_output') }}";

   var revAmount = [];
  $.getJSON(rev , function(json){
    for (var i = 0; i < json.length; i++) {
        revAmount.push(json[i].amount);
    }

  });
  // console.log(revAmount);

  $.getJSON(exp , function(json){
    console.log(json);
      var month = [], expAmount = [];
      for (var i = 0; i < json.length; i++) {
        month.push(json[i].month_name);
        expAmount.push(json[i].amount);
      }
    // var monthExp = [], monthRev = [], typeData = [], valueData = [];
    // var exp=[], rev=[];
    // console.log(listMonth.length);
    // for (var i = 0; i < listMonth.length; i++) {
    //
    //   for(var int = 0; int < result.length; int++){
    //     // console.log(result[int].type_trans);
    //     // console.log(result[int]);
    //     if(result[int].type_trans== "Expense"){
    //       if (listMonth[i] == result[int].month_name) {
    //         exp.push(result[int].amount)
    //       }else{
    //         exp.push(0);
    //        }
    //
    //       // monthExp.push(result[int].month_name)
    //       // valueData.push(dataExp);
    //     }
    //     // else{
    //     //   // monthRev.push(result[int].month_name);
    //     //   if (listMonth[i] == result[int].month_name) {
    //     //     rev.push(result[int].amount)
    //     //   }else{
    //     //     rev.push(0);
    //     //   }
    //     //   // rev.push(result[int].amount);
    //     //   // valueData.push(dataRev);
    //     // }
    //   }
    // }

    //console.log(exp);
  var value ={
  labels: listMonth,
  datasets: [{
      label: "Expense",
      fill: false,
      lineTension: 0.1,
      borderColor: "red", // The main line color
      borderCapStyle: 'square',
      borderDash: [], // try [5, 15] for instance
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "red",
      pointBackgroundColor: "white",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "red",
      pointHoverBorderColor: "white",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      showTooltip: false,
      data: expAmount,
      spanGaps: true,
    },
     {
      label: "Revenue",
      fill: false,
      lineTension: 0.1,
      borderColor: "#2980b9",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "#2980b9",
      pointBackgroundColor: "#FFF",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "#2980b9",
      pointHoverBorderColor: "#FFF",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      showTooltip: false,
      pointHitRadius: 10,
      data:revAmount,
      spanGaps: false,
    }

  ]
  };

// Notice the scaleLabel at the same level as Ticks
    var options = {
           animateScale : true,
    scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true,
                      callback: function(value, index, data){
                         value = value.toString();
                         value = value.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                         return value;
                      }
                  },
                  scaleLabel: {
                       display: true,
                       fontSize: 20
                    }
              }]
          }
    };

    // Chart declaration:
    var barChartMulti = document.getElementById('barChartMulti');

    var myBarChart = new Chart(barChartMulti, {
      type: 'line',
      data: value,
      options: options
    });
  });
  });
</script>

  <script type="text/javascript">
  var apiMonth = "{{ url('api/month') }}";
  $.getJSON(apiMonth ,function(json) {
      for(var i = 0; i < json.length; i++){
        var option = $('<option></option>');
        $('#selectMonth').append(option.val(json[i].id).text(json[i].month_name));
      }
  });

   $(document).ready(function(){

    $('#emp_hiring_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#start_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#end_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#invoice_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#paid_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
  });
</script>
</body>
</html>
