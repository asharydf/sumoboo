<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Model\Payroll;
use App\Model\CashbondPayroll;
use App\Model\FranchiseFee;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
  	{
    	$pending_payroll = Payroll::where('status', 0)->first();
    	$pending_cashbond_payroll = CashbondPayroll::where('status', 0)->first();
    	$pending_franchise_fee = FranchiseFee::where('status', 0)->first();

        $finance_payroll = Payroll::where('status', 1)->orWhere('status', 3)->first();
        $finance_cashbond_payroll = CashbondPayroll::where('status', 1)->orWhere('status', 3)->first();
        $finance_franchise_fee = FranchiseFee::where('status', 1)->orWhere('status', 3)->first();

        $owner_payroll = Payroll::where('status', 2)->first();
        $owner_cashbond_payroll = CashbondPayroll::where('status', 2)->first();
        $owner_franchise_fee = FranchiseFee::where('status', 2)->first();

    	if($pending_payroll)
    	{
    		\View::share('global_pending_payroll', 1);
    	}

    	if($pending_cashbond_payroll)
    	{
    		\View::share('global_pending_cashbond_payroll', 1);
    	}

    	if($pending_franchise_fee)
    	{
    		\View::share('global_pending_franchise_fee', 1);
    	}

        if($finance_payroll)
        {
            \View::share('global_finance_payroll', 1);
        }

        if($finance_cashbond_payroll)
        {
            \View::share('global_finance_cashbond_payroll', 1);
        }

        if($finance_franchise_fee)
        {
            \View::share('global_finance_franchise_fee', 1);
        }

        if($owner_payroll)
        {
            \View::share('global_owner_payroll', 1);
        }

        if($owner_cashbond_payroll)
        {
            \View::share('global_owner_cashbond_payroll', 1);
        }

        if($owner_franchise_fee)
        {
            \View::share('global_owner_franchise_fee', 1);
        }
  	}
}
