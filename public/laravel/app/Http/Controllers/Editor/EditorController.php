<?php

namespace App\Http\Controllers\Editor;
use Session;
use Illuminate\Http\Request;
use DB;
use App\Model\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Payroll;
use App\Model\Revenue;

class EditorController extends Controller
{
    public function index()
    {
        $user = User::first();
        // dd($user);
    	return view('editor.index');
    }

    public function revenue_output($month)
    {
      $years = date('Y');
      $revenue = Revenue::where('branch_id', Session::get('branch_id'))
              //  ->where('date', '>=', $date)
                ->whereMonth('date', '=', $month)

                ->whereYear('date', '=', $years)
                ->orderBy('date', 'desc')
                ->get();


      return $revenue;
    }

    public function month_output(){
      $month = DB::table('month')
      ->select('id', 'month_name')
      ->get();
      return $month;
    }


    public function expense_output_chart()
    {
      $sessionId = Session::get('branch_id');
      $expand = DB::select("
      SELECT
        *
        FROM
        (
            SELECT
                1 AS branch_id,
                month.id AS id_month,
                month.month_name AS month_name,
                IFNULL(SUM(DERIVEDTBL.amount), 0) AS amount
            FROM
                (
                    SELECT
                        *
                    FROM
                    (SELECT
    `cashbond`.`branch_id` AS `branch_id`,
    `cashbond`.`start_date` AS `date_tr`,
    ifnull(
        `cashbond`.`budget_issued`,
        0
    ) AS `amount`
FROM
    `cashbond`,
    `user`
WHERE
    (
        YEAR (`cashbond`.`start_date`) = `user`.`year`
    )
AND `user`.id = ".Auth::id()."
UNION ALL
    SELECT
        `invoice`.`branch_id` AS `branch_id`,
        `invoice`.`invoice_date` AS `date_tr`,
        (
            ifnull(
                `invoice`.`invoice_total`,
                0
            ) + ifnull(
                `invoice`.`additional_cost`,
                0
            )
        ) AS `amount`
    FROM
        `invoice`,
        `user`
    WHERE
        (
            YEAR (`invoice`.`invoice_date`) = `user`.`year`
        )
    AND `user`.id = ".Auth::id()."
    UNION ALL
        SELECT
            `invoice_direct`.`branch_id` AS `branch_id`,
            `invoice_direct`.`invoice_date` AS `date_tr`,
            (
                ifnull(
                    `invoice_direct`.`invoice_total`,
                    0
                ) + ifnull(
                    `invoice_direct`.`additional_cost`,
                    0
                )
            ) AS `amount`
        FROM
            `invoice_direct`,
            `user`
        WHERE
            (
                YEAR (
                    `invoice_direct`.`invoice_date`
                ) = `user`.`year`
            )
        AND `user`.id = ".Auth::id()."
        UNION ALL
            SELECT
                `cashbond_payroll`.`branch_id` AS `branch_id`,
                `cashbond_payroll`.`date` AS `date`,
                `cashbond_payroll_detail`.`cashbond` AS `cashbond`
            FROM
                (
                    `cashbond_payroll`
                    JOIN `cashbond_payroll_detail` ON (
                        (
                            `cashbond_payroll_detail`.`cashbond_payroll_id` = `cashbond_payroll`.`id`
                        )
                    )
                ),
                `user`
            WHERE
                (
                    YEAR (`cashbond_payroll`.`date`) = `user`.`year`
                )
            AND `user`.id = ".Auth::id()."
            UNION ALL
                SELECT
                    `payroll`.`branch_id` AS `branch_id`,
                    `payroll`.`created_at` AS `date_tr`,
                    `payroll_detail`.`slr_thp` AS `amount`
                FROM
                    (
                        `payroll`
                        JOIN `payroll_detail` ON (
                            (
                                `payroll_detail`.`payroll_id` = `payroll`.`id`
                            )
                        )
                    ),
                    `user`
                WHERE
                    (
                        (
                            YEAR (`payroll`.`created_at`) = `user`.`year`
                        )
                        AND isnull(`payroll`.`deleted_at`)
                        AND isnull(
                            `payroll_detail`.`deleted_at`
                        )
                    )
                AND `user`.id = ".Auth::id().") AS
                        view_expense
                    WHERE
                        view_expense.branch_id = 1
                ) AS DERIVEDTBL
            RIGHT JOIN month ON MONTHNAME(DERIVEDTBL.date_tr) = month .month_name
            GROUP BY
                DERIVEDTBL.branch_id,
                month.id,
                MONTHNAME(DERIVEDTBL.date_tr),
                month.month_name
            UNION ALL
                SELECT
                    2 AS branch_id,
                    month.id AS id_month,
                    month.month_name AS month_name,
                    IFNULL(SUM(DERIVEDTBL.amount), 0) AS amount
                FROM
                    (
                        SELECT
                            *
                        FROM
                            (SELECT
    `cashbond`.`branch_id` AS `branch_id`,
    `cashbond`.`start_date` AS `date_tr`,
    ifnull(
        `cashbond`.`budget_issued`,
        0
    ) AS `amount`
FROM
    `cashbond`,
    `user`
WHERE
    (
        YEAR (`cashbond`.`start_date`) = `user`.`year`
    )
AND `user`.id = ".Auth::id()."
UNION ALL
    SELECT
        `invoice`.`branch_id` AS `branch_id`,
        `invoice`.`invoice_date` AS `date_tr`,
        (
            ifnull(
                `invoice`.`invoice_total`,
                0
            ) + ifnull(
                `invoice`.`additional_cost`,
                0
            )
        ) AS `amount`
    FROM
        `invoice`,
        `user`
    WHERE
        (
            YEAR (`invoice`.`invoice_date`) = `user`.`year`
        )
    AND `user`.id = ".Auth::id()."
    UNION ALL
        SELECT
            `invoice_direct`.`branch_id` AS `branch_id`,
            `invoice_direct`.`invoice_date` AS `date_tr`,
            (
                ifnull(
                    `invoice_direct`.`invoice_total`,
                    0
                ) + ifnull(
                    `invoice_direct`.`additional_cost`,
                    0
                )
            ) AS `amount`
        FROM
            `invoice_direct`,
            `user`
        WHERE
            (
                YEAR (
                    `invoice_direct`.`invoice_date`
                ) = `user`.`year`
            )
        AND `user`.id = ".Auth::id()."
        UNION ALL
            SELECT
                `cashbond_payroll`.`branch_id` AS `branch_id`,
                `cashbond_payroll`.`date` AS `date`,
                `cashbond_payroll_detail`.`cashbond` AS `cashbond`
            FROM
                (
                    `cashbond_payroll`
                    JOIN `cashbond_payroll_detail` ON (
                        (
                            `cashbond_payroll_detail`.`cashbond_payroll_id` = `cashbond_payroll`.`id`
                        )
                    )
                ),
                `user`
            WHERE
                (
                    YEAR (`cashbond_payroll`.`date`) = `user`.`year`
                )
            AND `user`.id = ".Auth::id()."
            UNION ALL
                SELECT
                    `payroll`.`branch_id` AS `branch_id`,
                    `payroll`.`created_at` AS `date_tr`,
                    `payroll_detail`.`slr_thp` AS `amount`
                FROM
                    (
                        `payroll`
                        JOIN `payroll_detail` ON (
                            (
                                `payroll_detail`.`payroll_id` = `payroll`.`id`
                            )
                        )
                    ),
                    `user`
                WHERE
                    (
                        (
                            YEAR (`payroll`.`created_at`) = `user`.`year`
                        )
                        AND isnull(`payroll`.`deleted_at`)
                        AND isnull(
                            `payroll_detail`.`deleted_at`
                        )
                    )
                AND `user`.id = 1) AS view_expense
                        WHERE
                            view_expense.branch_id = 2
                    ) AS DERIVEDTBL
                RIGHT JOIN month ON MONTHNAME(DERIVEDTBL.date_tr) = month .month_name
                GROUP BY
                    DERIVEDTBL.branch_id,
                    month.id,
                    MONTHNAME(DERIVEDTBL.date_tr),
                    month.month_name
        ) AS DERIVEDTBL1
        WHERE DERIVEDTBL1.branch_id = '$sessionId'
        ORDER BY
        DERIVEDTBL1.id_month
      ");
      return $expand;
    }

    public function revenue_output_chart()
    {
      $sessionId = Session::get('branch_id');
      $revenue = DB::select("
     SELECT
    '1' as branch_id,
    derivedtbl.`month` as id_month,
    derivedtbl.`month_name`,
    IFNULL(
        derivedtbl.`year`,
        YEAR (NOW())
    ) AS `year`,
    sum(derivedtbl.amount) AS amount
FROM
    (
        SELECT
            item_central.branch_id,
            `month`.month_name,
            'Suplay Sumoboo' AS `description`,
            `month`.`id` AS `month`,
            YEAR (`item_central`.`date`) AS `year`,
            sum(
                (
                    (
                        ifnull(
                            `item_central`.`total_item_central`,
                            0
                        ) + ifnull(
                            `item_central`.`total_invoice`,
                            0
                        )
                    ) + ifnull(
                        `item_central`.`total_transport`,
                        0
                    )
                )
            ) AS `amount`
        FROM
            `month`
        LEFT JOIN `item_central` ON `month`.id = MONTH (`item_central`.`date`)
        GROUP BY
            `item_central`.`branch_id`,
            MONTH (`item_central`.`date`),
            YEAR (`item_central`.`date`),
            `month`.month_name
        UNION ALL
            SELECT
                `invoice_direct`.`branch_id`,
                `month`.month_name,
                'Consumable direct to vendor' AS `type`,
                `month`.`id` AS `month`,
                YEAR (
                    `invoice_direct`.`invoice_date`
                ) AS `year`,
                sum(
                    `invoice_direct`.`invoice_total`
                ) AS `amount`
            FROM
                `month`
            LEFT JOIN `invoice_direct` ON `month`.id = MONTH (
                `invoice_direct`.`invoice_date`
            )
            GROUP BY
                `invoice_direct`.`branch_id`,
                MONTH (
                    `invoice_direct`.`invoice_date`
                ),
                YEAR (
                    `invoice_direct`.`invoice_date`
                ),
                `month`.month_name
            UNION ALL
                SELECT
                    `invoice`.`branch_id`,
                    `month`.month_name,
                    'Consumable (supporting restorant)' AS `type`,
                    `month`.`id` AS `month`,
                    YEAR (`invoice`.`invoice_date`) AS `year`,
                    sum(`invoice`.`invoice_total`) AS `amount`
                FROM
                    `month`
                LEFT JOIN `invoice` ON `month`.id = MONTH (`invoice`.`invoice_date`)
                GROUP BY
                    `invoice`.`branch_id`,
                    MONTH (`invoice`.`invoice_date`),
                    YEAR (`invoice`.`invoice_date`),
                    `month`.month_name
    ) AS derivedtbl
GROUP BY
    derivedtbl.`branch_id`,
    derivedtbl.`month`,
    derivedtbl.`month_name`
ORDER BY
    derivedtbl.`month` ASC
      ");
      return $revenue;
    }
}
