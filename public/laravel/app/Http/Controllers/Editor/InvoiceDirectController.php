<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\InvoiceDirectRequest;
use App\Http\Controllers\Controller;
use App\Model\InvoiceDirect;
use App\Model\Vendor;
use App\Model\InvoiceType;
use App\Model\Item;
use App\Model\InvoiceDirectDetail;
use App\Model\Branch;
use Intervention\Image\Facades\Image;

class InvoiceDirectController extends Controller
{

	public function index()
	{
		if (Input::has('page'))
		{
			$page = Input::get('page');
		}
		else
		{
			$page = 1;
		}
		$no = 15*$page-14;
		$invoicedirects = InvoiceDirect::where('branch_id', Session::get('branch_id'))
		->where('paid', null)
		->orderBy('created_at', 'DESC')
		->paginate(15);

		return view ('editor.invoice_direct.index', compact('invoicedirects'))->with('number',$no);
	}

	public function indexbank()
	{
		if (Input::has('page'))
		{
			$page = Input::get('page');
		}
		else
		{
			$page = 1;
		}
		$no = 15*$page-14;
		$invoicedirects = InvoiceDirect::where('branch_id', Session::get('branch_id'))
		->where('paid', 1)
		->orderBy('created_at', 'DESC')
		->paginate(15);

		return view ('editor.invoice_direct.bank', compact('invoicedirects'))->with('number',$no);
	}

	public function create()
	{
		$vendor_list = Vendor::all()->pluck('vendor_name', 'id');
		$invoice_type_list = InvoiceType::where('item_category_id', 1)->pluck('inv_type_name', 'id');
		$item_list = Item::where('item_category_id', 8)->pluck('item_name', 'id');
		$invoice_detail = InvoiceDirectDetail::where('invoice_direct_id', '')->get();

		return view ('editor.invoice_direct.form', compact('invoicedirect', 'vendor_list', 'invoice_type_list', 'item_list', 'invoice_detail'));
	}

	public function store(InvoiceDirectRequest $request)
	{
		// DB::beginTransaction();
		// try {
            //get last transaction id

		$invoicedirect = new InvoiceDirect;
		$invoicedirect->invoice_type_id = $request->input('invoice_type_id');
		$invoicedirect->invoice_date = $request->input('invoice_date');
		$invoicedirect->vendor_id = $request->input('vendor_id');
		$invoicedirect->invoice_bank = $request->input('invoice_bank');
		$invoicedirect->invoice_rekening = $request->input('invoice_rekening');
		$invoicedirect->additional_cost = $request->input('additional_cost');
		$invoicedirect->add_cost_desc = $request->input('add_cost_desc');
		$invoicedirect->invoice_total = $request->input('invoice_total');
		$invoicedirect->comment = $request->input('comment');
		$invoicedirect->created_by = Auth::id();
		$invoicedirect->branch_id = Session::get('branch_id');
		$invoicedirect->save();

		if($request->image)
		{
			$invoicedirect = InvoiceDirect::FindOrFail($invoicedirect->id);

			$original_directory = "uploads/invoicedirect/";

			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$invoicedirect->invoice_attachment = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
			$request->image->move($original_directory, $invoicedirect->invoice_attachment);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$invoicedirect->invoice_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$invoicedirect->invoice_attachment);

			$invoicedirect->save();
		}

		$cart = $request->input('item_id');
		if($cart != null)
		{
			foreach($request->input('item_id') as $key => $quantitydetail)
			{
				$invoice_detail = new InvoiceDirectDetail;
				$invoice_detail->item_id = $request->input('item_id.'.$key);
				$invoice_detail->quantity = $request->input('quantity.'.$key);
				$invoice_detail->uom = $request->input('uom.'.$key);
				$invoice_detail->price = $request->input('price.'.$key);
				$invoice_detail->total = $request->input('total.'.$key);
				$invoice_detail->invoice_direct_id = $invoicedirect->id;
				$invoice_detail->save();
			}
		}

		return redirect('editor/invoicedirect');
		// } catch (\Exception $e) {
		// 	DB::rollback();
		// }

	}

	public function edit($id)
	{
		$vendor_list = Vendor::all()->pluck('vendor_name', 'id');
		$invoice_type_list = InvoiceType::all()->pluck('inv_type_name', 'id');
		$invoicedirect = InvoiceDirect::Find($id);
		$item_list = Item::Pluck('item_name', 'id');

		$invoice_detail = InvoiceDirectDetail::where('invoice_direct_id', $id)->get();
		//dd($invoice_detail);

		return view ('editor.invoice_direct.form', compact('invoicedirect', 'vendor_list', 'invoice_type_list', 'invoice_detail', 'item_list'));
	}

	public function view($id)
	{
		$vendor_list = Vendor::all()->pluck('vendor_name', 'id');
		$invoice_type_list = InvoiceType::all()->pluck('inv_type_name', 'id');
		$invoicedirect = InvoiceDirect::Find($id);
		$item_list = Item::Pluck('item_name', 'id');

		//$invoice_detail = InvoiceDirectDetail::where('invoice_direct_id', $id)->get();

		$invoice_detail = DB::table('invoice_direct_detail')
		->leftjoin('item', 'invoice_direct_detail.item_id', '=', 'item.id')
		->select('invoice_direct_detail.id',
			'invoice_direct_detail.invoice_direct_id',
			'invoice_direct_detail.item_id',
			'item.item_name',
			'invoice_direct_detail.quantity',
			'invoice_direct_detail.price',
			'invoice_direct_detail.uom',
			'invoice_direct_detail.total')
		->where('invoice_direct_detail.invoice_direct_id', '=', $id)
		->get();

			//dd($invoice_detail);

		return view ('editor.invoice_direct.view', compact('invoicedirect', 'vendor_list', 'invoice_type_list', 'invoice_detail', 'item_list'));
	}

	public function update($id, InvoiceDirectRequest $request)
	{
		// DB::beginTransaction();
		// try {

		//InvoiceDirect::where('id', $id)->delete();

		$invoicedirect = InvoiceDirect::Find($id);
		$invoicedirect->invoice_type_id = $request->input('invoice_type_id');
		$invoicedirect->invoice_date = $request->input('invoice_date');
		$invoicedirect->vendor_id = $request->input('vendor_id');
		$invoicedirect->invoice_bank = $request->input('invoice_bank');
		$invoicedirect->invoice_rekening = $request->input('invoice_rekening');
		$invoicedirect->additional_cost = $request->input('additional_cost');
		$invoicedirect->add_cost_desc = $request->input('add_cost_desc');
		$invoicedirect->invoice_total = $request->input('invoice_total');
		$invoicedirect->paid_date = $request->input('paid_date');
		$invoicedirect->reference_no =  $request->input('reference_no');
		$invoicedirect->comment = $request->input('comment');
		$invoicedirect->save();

		if($request->image)
		{
			$invoicedirect = InvoiceDirect::FindOrFail($invoicedirect->id);

			$original_directory = "uploads/invoicedirect/";

			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$invoicedirect->invoice_attachment = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
			$request->image->move($original_directory, $invoicedirect->invoice_attachment);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$invoicedirect->invoice_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$invoicedirect->invoice_attachment);

			$invoicedirect->save();
		}

		InvoiceDirectDetail::where('invoice_direct_id', $id)->delete();

		$cart = $request->input('item_id');
		if($cart != null)
		{
			foreach($request->input('item_id') as $key => $quantitydetail)
			{
				$invoice_detail = new InvoiceDirectDetail;
				$invoice_detail->item_id = $request->input('item_id.'.$key);
				$invoice_detail->quantity = $request->input('quantity.'.$key);
				$invoice_detail->uom = $request->input('uom.'.$key);
				$invoice_detail->price = $request->input('price.'.$key);
				$invoice_detail->total = $request->input('total.'.$key);
				$invoice_detail->invoice_direct_id = $invoicedirect->id;
				$invoice_detail->save();
			}
		}


		// 	DB::commit();

		return redirect('editor/invoicedirect');
		// } catch (\Exception $e) {
		// 	DB::rollback();
		// }
	}

	public function approval($id)
	{
		$vendor_list = Vendor::all()->pluck('vendor_name', 'id');
		$invoice_type_list = InvoiceType::all()->pluck('inv_type_name', 'id');
		$invoicedirect = InvoiceDirect::Find($id);
		$item_list = Item::Pluck('item_name', 'id');

		//$invoice_detail = InvoiceDirectDetail::where('invoice_direct_id', $id)->get();

		$invoice_detail = DB::table('invoice_direct_detail')
		->leftjoin('item', 'invoice_direct_detail.item_id', '=', 'item.id')
		->select('invoice_direct_detail.id',
			'invoice_direct_detail.invoice_direct_id',
			'invoice_direct_detail.item_id',
			'item.item_name',
			'invoice_direct_detail.quantity',
			'invoice_direct_detail.price',
			'invoice_direct_detail.uom',
			'invoice_direct_detail.total')
		->where('invoice_direct_detail.invoice_direct_id', '=', $id)
		->get();

			//dd($invoice_detail);

		return view ('editor.invoice_direct.approval', compact('invoicedirect', 'vendor_list', 'invoice_type_list', 'invoice_detail', 'item_list'));
	}

	public function updaterequset($id)
	{
		$now = Carbon::now();
		$invoicedirect = InvoiceDirect::Find($id);
		$invoicedirect->approved = 1;
		$invoicedirect->approved_date = $now;
		$invoicedirect->save();

		return redirect()->action('Editor\InvoiceDirectController@index');
	}

	public function updatepaid($id, Request $request)
	{
		$now = Carbon::now();
		$invoicedirect = InvoiceDirect::Find($id);
		$invoicedirect->paid = 1;
		$invoicedirect->paid_date = $now;
		$invoicedirect->paid_date = $request->input('paid_date');
		$invoicedirect->reference_no =  $request->input('reference_no');
		$invoicedirect->save();

		if($request->invoice_receip)
		{
			$invoicedirect = InvoiceDirect::FindOrFail($invoicedirect->id);

			$original_directory = "uploads/invoicedirect/";

			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$invoicedirect->invoice_receip = Carbon::now()->format("d-m-Y h-i-s").$request->invoice_receip->getClientOriginalName();
			$request->invoice_receip->move($original_directory, $invoicedirect->invoice_receip);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$invoicedirect->invoice_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$invoicedirect->invoice_attachment);

			$invoicedirect->save();
		}

		return redirect()->action('Editor\InvoiceDirectController@index');
	}
	public function delete($id)
	{
		InvoiceDirect::Find($id)->delete();
		return redirect()->action('Editor\InvoiceDirectController@index');
	}


}
