<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemCentralImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ 
            'import_file' => 'required|max:5000|mimes:ods',
        ];
    }

    public function messages()
    {
        return [ 
            'import_file.required' => 'Attachment is required', 
            'import_file.max' => 'Maximum file size is 5 MB', 
        ];
    }
}
