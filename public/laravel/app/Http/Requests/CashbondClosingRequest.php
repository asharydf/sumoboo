<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CashbondClosingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ 
            'image' => 'required|max:5000', 
            'date' => 'required', 
            'nota_number' => 'required', 
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Date is required', 
            'nota_number.required' => 'Nota number is required', 
            'image.required' => 'Attachment is required', 
            'image.max' => 'Maximum file size is 5 MB',  
        ];
    }
}
