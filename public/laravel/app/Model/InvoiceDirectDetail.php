<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceDirectDetail extends Model
{
    protected $table = 'invoice_direct_detail';

    public function invoice()
	{
		return $this->belongsTo('App\Model\InvoiceDirect', 'invoice_direct_id', 'id');
	}
}



