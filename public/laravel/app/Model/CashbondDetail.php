<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CashbondDetail extends Model
{
    protected $table = 'cashbond_detail';

    public function cashbond()
	{
		return $this->belongsTo('App\Model\Cashbond', 'cashbond_id', 'id');
	}
}



