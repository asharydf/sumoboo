<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Op extends Model
{
    use SoftDeletes;

    protected $table = "op";
    protected $dates = ['deleted_at'];
}
