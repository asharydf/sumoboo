<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
	use SoftDeletes;
	protected $table = 'item';
	protected $dates = ['deleted_at'];

	public function item_category()
	{
		return $this->belongsTo('App\Model\ItemCategory', 'item_category_id', 'id');
	}

	public function cashbond_detail_item()
	{
		return $this->belongsTo('App\Model\CashbondDetailItem', 'item_id', 'id');
	}

	public function vendor_item()
	{
		return $this->belongsTo('App\Model\VendorItem', 'item_id', 'id');
	}

}
