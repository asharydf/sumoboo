<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CashOPDetail extends Model
{
    protected $table = 'cash_operational_detail';

    public function cashbond()
	{
		return $this->belongsTo('App\Model\CashOP', 'cash_operational_id', 'id');
	}
}



